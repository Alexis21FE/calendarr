<?php

namespace App;
use App\Pacientes;
use Illuminate\Database\Eloquent\Model;

class datos_significativos extends Model
{
    public function paciente(){
    	return $this->belongsTo(Pacientes::class,'paciente_id');
    }
}
