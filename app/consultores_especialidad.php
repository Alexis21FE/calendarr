<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Consultores;
class consultores_especialidad extends Model
{
    public function consultores(){
    	return $this->belongTo(Consultores::class,'consultor_id');
    }
}
