<?php

namespace App;
use App\Citas;
use App\Beca_descuento;
use Illuminate\Database\Eloquent\Model;

class beca_cita extends Model
{
    protected $table='beca_citas';
    protected $fillable=['cantidad','equivalente','monto'];

    public function citas_reg(){
    	return $this->belongsTo(Citas::class,'cita_id');
    }
    public function becas_reg(){
    	return $this->belongsTo(Beca_descuento::class,'beca_id');
    }
}
