<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Consultores;
class consultor_categoria extends Model
{
    //
    protected $table='consultor_categorias';
      public function categorias(){
    return $this->hasOne(Consultores::class,'categoria_id');
  }
    
}
