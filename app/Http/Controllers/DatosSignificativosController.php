<?php

namespace App\Http\Controllers;

use App\datos_significativos;
use Illuminate\Http\Request;

class DatosSignificativosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\datos_significativos  $datos_significativos
     * @return \Illuminate\Http\Response
     */
    public function show(datos_significativos $datos_significativos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\datos_significativos  $datos_significativos
     * @return \Illuminate\Http\Response
     */
    public function edit(datos_significativos $datos_significativos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\datos_significativos  $datos_significativos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, datos_significativos $datos_significativos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\datos_significativos  $datos_significativos
     * @return \Illuminate\Http\Response
     */
    public function destroy(datos_significativos $datos_significativos)
    {
        //
    }
}
