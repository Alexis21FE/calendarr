<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipo_citas;
use App\Pacientes;
use App\Consultorio;
use App\Consultores;
use App\Especialidades;
use App\consultor_categoria;
use App\Beca_descuento;
use App\status_cita;
use App\Citas;
use Session;
use App\apoyo_registro;
use DB;
use App\consultores_especialidad;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','roles:admin,direc,recep']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function director_consultor(Request $request){
if($request->calendario_type=='consultor'){
            Session::put('con_si',true);
        }else{
            Session::put('con_si',false);
        }
       return response()->json([
    'status' => true
]);
    }
    public function index()
    {
    $tipo_citas=Tipo_citas::orderBy('id')->pluck('descripcion', 'id');
     $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where('casos.status','!=','Alta')
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
    $consultorios=Consultorio::orderBy('id')->pluck('descripcion', 'id');
    $status_cita=status_cita::orderBy('id')->pluck('descripcion','id');
    $consultores=Consultores::with('Especialidades')->where('activo','A')->get();
    $becas_descuentos=Beca_descuento::where('id','!=',2)->orderBy('id')->get();
    $porcentaje_becas=range(0, 100, 10);
    if(auth()->user()->role->name=='direc'){
    $consultor_id=auth()->user()->consultor->id;
  
         return view('director.calendario',compact('tipo_citas','pacientes','porcentaje_becas','consultorios','consultores','consultores_select','becas_descuentos','status_cita','consultor_id'));
    }
        return view('/home',compact('tipo_citas','pacientes','porcentaje_becas','consultorios','consultores','consultores_select','becas_descuentos','status_cita'));
    
    }

    public function categoria(Request $request){

        $categoria_valor=consultor_categoria::where('id',$request->categoria_id)->first();

         return response()->json([
    'costo' => $categoria_valor,
    'status' => true
]);
    }
    public function becas(Request $request){
        $id=$request->beca_id;

        $beca=Beca_descuento::where('id',$id)->first();
        if($beca->tiene_saldo!='S'){
        return response()->json([
    'beca' => $beca,
    'status' => true
]);
        }else{
            $beca=Beca_descuento::with('apoyos_afecta')->where('id',$id)->first();
          return response()->json([
            'beca'=>$beca,
            'status'=>true
          ]);
        }
    }
     public function calculaBecas(Request $request){
        if($request->becas!=''){
            $preciofinal=$request->costo__;
            foreach ($request->becas as $beca ) {

                $afecta=$beca['afecta'];
                $monto_porcentaje=$beca['monto_porcentaje'];
                if($afecta=="ambos"){
                    if($monto_porcentaje=="monto"){
                        $monto=$beca['cantidad'];
                        $preciofinal=$preciofinal-$monto;

                    }elseif ($monto_porcentaje=="porcentaje") {
                        $porcentaje=$beca['cantidad'];
                        $porcentaje_aux=100-$porcentaje;
                        $porcentaje=$porcentaje_aux/100;
                        $preciofinal=$porcentaje*$preciofinal;
                    }

                }elseif ($afecta=="institucion") {
                    if ($monto_porcentaje=="porcentaje") {
                        $porcentaje=$beca['cantidad'];
                        $porcentaje_aux=100-$porcentaje;
                        $porcentaje=$porcentaje_aux/100;
                        $preciofinal=$porcentaje*$preciofinal;
                    }
                    # code...
                }elseif ($afecta=="consultor") {

                        if($monto_porcentaje=="monto"){
                        $monto=$beca['cantidad'];
                        $preciofinal=$preciofinal-$monto;
                    }
                }
                
            }
        }else{

            $preciofinal=$request->costo__;
        }   
        return response()->json([
    'precio' => $preciofinal,
    'status' => true
]);
    }
    public function checkConsultorio(Request $request){
                $fecha=$request->fecha;
       $start=$fecha." ".$request->start;
       $end=$fecha." ".$request->end;
       $end=date_create($end);
       date_modify($end,"+1 hour");
       $end=date_format($end,"Y-m-d H:m:s");
       $start=date_create($start);
       date_modify($start,"-1 hour");
           $start=date_format($start,"Y-m-d H:m:s");

        $consultorio_id=Citas::where('consultorio_id',$request->consultorio_id)->whereBetween('start', [$start, $end])->get();
        
        if($consultorio_id->count()>0){
                    return response()->json([
    'status' => true
]);

        }
    }
    public function consultoreShow(){
        $consultores=Consultores::where('activo','A')->get();
        return view('consultores/showAll',compact('consultores'));
    }
    public function showConsultor(Request $request){
        $dias=array('lunes','martes','miercoles','jueves','viernes','sabado');
    $especialidades=Especialidades::orderBy('id')->get();
    $categorias=consultor_categoria::orderBy('id')->pluck('descripcion', 'id');
        $consultor=Consultores::with('Especialidades','categorias')->where([['activo','A'],['id',$request->consultor_id]])->first();
        $isResponse=true;
        return view('consultores.responseConsultor',compact('consultor','especialidades','categorias','isResponse','dias'));
    }
  
   public function showEspecialidades(){
    $especialidades=Especialidades::orderBy('id')->pluck('descripcion','id');
    return view('consultores.especialidades',compact('especialidades'));
   }
   public function showEsp_Con(Request $request){
    $especialidad_id=$request->especialidad_id;
    $especialidades_response=Especialidades::with('consultores')->where('id',$especialidad_id)->get();
    $especialidades=Especialidades::orderBy('id')->pluck('descripcion','id');
    $isResponse=true;
    return view('consultores.especialidades',compact('especialidades','isResponse','especialidades_response'));
     }
}
