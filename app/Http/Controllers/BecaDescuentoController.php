<?php

namespace App\Http\Controllers;

use App\Beca_descuento;
use Illuminate\Http\Request;

class BecaDescuentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Beca_descuento  $beca_descuento
     * @return \Illuminate\Http\Response
     */
    public function show(Beca_descuento $beca_descuento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Beca_descuento  $beca_descuento
     * @return \Illuminate\Http\Response
     */
    public function edit(Beca_descuento $beca_descuento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Beca_descuento  $beca_descuento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Beca_descuento $beca_descuento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Beca_descuento  $beca_descuento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Beca_descuento $beca_descuento)
    {
        //
    }
}
