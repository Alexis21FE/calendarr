<?php

namespace App\Http\Controllers;

use App\consultores_especialidad;
use Illuminate\Http\Request;

class ConsultoresEspecialidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\consultores_especialidad  $consultores_especialidad
     * @return \Illuminate\Http\Response
     */
    public function show(consultores_especialidad $consultores_especialidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\consultores_especialidad  $consultores_especialidad
     * @return \Illuminate\Http\Response
     */
    public function edit(consultores_especialidad $consultores_especialidad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\consultores_especialidad  $consultores_especialidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, consultores_especialidad $consultores_especialidad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\consultores_especialidad  $consultores_especialidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(consultores_especialidad $consultores_especialidad)
    {
        //
    }
}
