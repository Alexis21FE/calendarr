<?php

namespace App\Http\Controllers;

use App\apoyos;
use Illuminate\Http\Request;

class ApoyosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\apoyos  $apoyos
     * @return \Illuminate\Http\Response
     */
    public function show(apoyos $apoyos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\apoyos  $apoyos
     * @return \Illuminate\Http\Response
     */
    public function edit(apoyos $apoyos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\apoyos  $apoyos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, apoyos $apoyos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\apoyos  $apoyos
     * @return \Illuminate\Http\Response
     */
    public function destroy(apoyos $apoyos)
    {
        //
    }
}
