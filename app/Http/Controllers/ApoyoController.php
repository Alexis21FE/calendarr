<?php

namespace App\Http\Controllers;

use App\apoyo;
use Illuminate\Http\Request;

class ApoyoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function show(apoyo $apoyo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function edit(apoyo $apoyo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, apoyo $apoyo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function destroy(apoyo $apoyo)
    {
        //
    }
}
