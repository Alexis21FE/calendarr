<?php

namespace App\Http\Controllers;

use App\datos_factura;
use Illuminate\Http\Request;

class DatosFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\datos_factura  $datos_factura
     * @return \Illuminate\Http\Response
     */
    public function show(datos_factura $datos_factura)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\datos_factura  $datos_factura
     * @return \Illuminate\Http\Response
     */
    public function edit(datos_factura $datos_factura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\datos_factura  $datos_factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, datos_factura $datos_factura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\datos_factura  $datos_factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(datos_factura $datos_factura)
    {
        //
    }
}
