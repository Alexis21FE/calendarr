<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\consultor_categoria;
use App\Especialidades;
use App\apoyo;
use App\Beca_descuento;
use App\apoyo_registros;
use App\Consultorio;
use App\User;
use App\Mail\SendMailable;
use App\Role;
use App\sedes;
use Mail;
use App\Mail\passwordMail;
class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response

     */
    public function __construct()
    {
        $this->middleware(['auth','roles:admin,direc']);
    }
    public function sedes(){
        $sedes=sedes::orderby('id','desc')->get();
        return view('Admin.configuraciones.sedes',compact('sedes'));
    }

    public function index()
    {
        //
    }
    public function usuarios(){
        $usuarios=User::with('role')->where('role_id','!=','3')->get();
        $roles=Role::where('id','!=','3')->get();
        return view('Admin.configuraciones.usuarios',compact('usuarios','roles'));
    }
    public function datos(){
        $user=User::where('id',auth()->user()->id)->first();
         $roles=Role::where('id','!=','3')->get();
        return view('Admin.user',compact('user','roles'));
    }
    public function users(Request $request){
      $user_id=$request->usuario_id;
      $usuario=User::where('id',$user_id)->first();
        return response()->json([
        'usuario' =>$usuario,
        'status' => true
    ]);
        

    }
     public function usuariosSotre(Request $request){
        if($request->usuario_id!=''){
             $user=User::where('id',$request->usuario_id)->first();
            $user->email=$request->email;
            $user->role_id=$request->role;
            $user->activo=$request->activo;
            if($request->pass!=''){
            $user->password=bcrypt($request->pass);
            }
            $user->name=$request->usuario;
            $user->save();


              $mensaje="Exito al actualizar el usuario";
            $status=true;
        }else{
        
 $user=new User();

            $user->email=$request->email;
            $email=$request->email;
            $user->role_id=$request->role;
            $user->activo=$request->activo;
     $pass = substr( md5(microtime()), 1, 8);
            $user->password=bcrypt($pass);
            $user->name=$request->usuario;
            $user->save();  
            $info = array();
$info = array_add($info, 'pass' , $pass);
$info = array_add($info, 'email',$email);
     Mail::to($email)->bcc("family.recepcion@gmail.com")->send(new passwordMail($info));
            $mensaje="Exito al agregar el nuevo usuario";
            $status=true;
}
   
        return response()->json([
        'messagge' =>$mensaje,
        'status' => $status,
        'user'=>$user
    ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function Fondos(){
        $fondos=apoyo::with('registro')->get();
        return view('Admin.configuraciones.fondos',compact('fondos'));
    }
    public function fondoUpdate(Request $request){

       $fondo_registro=new apoyo_registros;
       $fondo_registro->cita_id=0;
       $fondo_registro->user_id=auth()->user()->id;
       $fondo_registro->beca_id=$request->beca_id;
       $fondo_registro->cargo=0;
       $fondo_registro->apoyo_id=$request->fondo_id;
       $fondo_registro->abono=$request->saldo_abonar;
       $fondo_old=apoyo_registros::where('apoyo_id',$fondo_registro->apoyo_id)->orderBy('id','desc')->first();
       $fondo_registro->saldo_inicial=$fondo_old->saldo_final;
     $fondo_registro->saldo_final=$fondo_old->saldo_final + $request->saldo_abonar;
       $fondo_registro->save();
       return redirect()->route('configuracion.fondos');
   }
   public function fondoNew(Request $request){

    $fondo=new apoyo;
    $fondo->descripcion=$request->fondo;
    $fondo->save();
    $fondo_registro=new apoyo_registros;
    $fondo_registro->cita_id=0;
    $fondo_registro->beca_id=0;
    $fondo_registro->apoyo_id=$fondo->id;
    $fondo_registro->cargo=0;
    $fondo_registro->saldo_inicial=0;
    $fondo_registro->abono=$request->Saldo_inicial;
    $fondo_registro->saldo_final=$request->Saldo_inicial;

    if($fondo_registro->save()){
     return response()->json([
        'mensaje' =>'Exito al actulizar la especialidad',
        'status' => true,
        'fondo_registro'=>$fondo_registro,
        'fondo'=>$fondo
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar la especialidad',
        'status' => false
    ]);

}
}
public function categorias(){
    $categorias=consultor_categoria::orderBy('descripcion')->get();
    return view('Admin.configuraciones.categorias',compact('categorias'));
}
public function especialidades(){
    $especialidades=Especialidades::all();
    return view('Admin.configuraciones.especialidades',compact('especialidades'));
}
public function especialidadUpdate(Request $request){
    $especialidad=Especialidades::where('id',$request->especialidad_id)->first();
    $especialidad->descripcion=$request->especialidad;

    if($especialidad->save()){
     return response()->json([
        'mensaje' =>'Exito al actulizar la especialidad',
        'status' => true
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar la especialidad',
        'status' => false
    ]);
}
}
public function especialidadNew(Request $request){
    $especialidad=new Especialidades();
    $especialidad->descripcion=$request->especialidad;
    $especialidad->fecha_modificacion=date("Y-m-d");
    if($especialidad->save()){
     return response()->json([
        'mensaje' =>'Exito al actulizar la especialidad',
        'status' => true,
        'especialidad'=>$especialidad
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar la especialidad',
        'status' => false
    ]);
}
}
public function sedesUpdate(Request $request){
    $especialidad=sedes::where('id',$request->sede_id)->first();
    $especialidad->descripcion=$request->sede;

    if($especialidad->save()){
     return response()->json([
        'mensaje' =>'Exito al actulizar la sede',
        'status' => true
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar la sede',
        'status' => false
    ]);
}
}
public function sedesNew(Request $request){
    $especialidad=new sedes();
    $especialidad->descripcion=$request->sede;
    $especialidad->fecha_modificacion=date("Y-m-d");
    if($especialidad->save()){
     return response()->json([
        'mensaje' =>'Exito al actulizar la especialidad',
        'status' => true,
        'sede'=>$especialidad
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar la especialidad',
        'status' => false
    ]);
}
}
public function becas_apoyos(){
    $cero=0;
    $apoyos_sin_beca=apoyo::whereHas('registro', function ($query) use ($cero) {
       $query->where('beca_id',$cero);
   })->get();
    $apoyos=apoyo::all();
    $becas_apoyos=Beca_descuento::with('apoyos_afecta')->get();
       // dd($becas_apoyos->toArray());
    return view('Admin.configuraciones.becas_apoyos',compact('apoyos','apoyos_sin_beca','becas_apoyos'));
}
public function becaUpdate(Request $request){
    $beca_apoyo=Beca_descuento::where('id',$request->beca_id)->first();
    $beca_apoyo->descripcion=$request->Descripcion;
    $beca_apoyo->monto_porcentaje=$request->Porcentaje_monto;
    $beca_apoyo->tiene_saldo=$request->tiene_saldo;
    $beca_apoyo->orden=$request->orden;
    $beca_apoyo->afecta=$request->afecta;

    if($beca_apoyo->save()){
        
    return response()->json([
        'mensaje' =>'Exito al actulizar la especialidad',
        'status' => true,
        'beca_apoyo'=>$beca_apoyo
    ]);
}else{
    return response()->json([
        'mensaje' =>'Error al actulizar la especialidad',
        'status' => false
    ]);
}
}
public function becaNew(Request $request){
    $beca_apoyo= new Beca_descuento;
    $beca_apoyo->descripcion=$request->Descripcion;
    $beca_apoyo->monto_porcentaje=$request->Porcentaje_monto;
    $beca_apoyo->tiene_saldo=$request->tiene_saldo;
    $beca_apoyo->orden=$request->orden;
    $beca_apoyo->afecta=$request->afecta;
    $apoyos=apoyo_registros::where('apoyo_id',$request->nombre_fondo)->first();
    if($beca_apoyo->save()){
        if($beca_apoyo->tiene_saldo=='S'){
         $apoyos->beca_id=$beca_apoyo->id;
         $apoyos->save();
         $nombre_apoyo=$apoyos->descripcion;
     }

 }
 return redirect()->route('configuracion.becas_apoyos');
}
public function Consultorios(){
    $consultorios=Consultorio::all();
    return view('Admin.configuraciones.Consultorios',compact('consultorios'));
}

public function consultorioUpdate(Request $request){
    $consultorio=Consultorio::where('id',$request->consultorio_id)->first();
    $consultorio->descripcion=$request->consultorio;
    if($consultorio->save()){
     return response()->json([
        'mensaje' =>'Exito al actulizar el consultorio',
        'status' => true,
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar el consultorio',
        'status' => false
    ]);
}
}
public function consultorioNew(Request $request){
    $consultorio=new Consultorio;
    $consultorio->descripcion=$request->consultorio;
    $consultorio->fecha_de_modificacion=date("Y-m-d");
        if($consultorio->save()){
     return response()->json([
        'mensaje' =>'Exito al agregar el consultorio',
        'status' => true,
        'consultorio'=>$consultorio
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al agregar el consultorio',
        'status' => false
    ]);
}
}

public function categoriaUpdate(Request $request){
    $categoria=consultor_categoria::where('id',$request->categoria_id)->first();
    $categoria->descripcion= $request->categoria;
    $categoria->Porcentaje_consultor= $request->Porcentaje_consultor;
    $categoria->Porcentaje_family= $request->Porcentaje_family;   
    if($categoria->save()){

     return response()->json([
        'mensaje' =>'Exito al actulizar la categoria',
        'status' => true
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar la categoria',
        'status' => false
    ]);
}
}
public function categoriaNew(Request $request){
    $categoria=new consultor_categoria();
    $categoria->descripcion=$request->categoria;
    $categoria->Porcentaje_consultor= $request->Porcentaje_consultor;
    $categoria->Porcentaje_family= $request->Porcentaje_family;
    $categoria->fecha_modificacion=date("Y-m-d"); 
    if($categoria->save()){
     return response()->json([
        'mensaje' =>'Exito al actulizar la categoria',
        'status' => true,
        'categoria'=>$categoria
    ]);
 }else{
    return response()->json([
        'mensaje' =>'Error al actulizar la categoria',
        'status' => false
    ]);
}
}
}

