<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Citas;
class Consultorio extends Model
{
	protected $table='consultorios';
	protected $fillable=['descripcion'];
    //
    public function citas(){
        return $this->hasMany(Citas::class, 'consultorio_id');
    }
}
