<?php

namespace App;
use App\Diagnostico;
use Illuminate\Database\Eloquent\Model;

class clasificacion extends Model
{
    protected $table='clasificacions';
     public function diagnostico(){
    	return $this->hasMany(Diagnostico::class,'diagnostico_id');
    }
}
