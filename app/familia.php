<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pacientes;
use App\Caso;
class familia extends Model
{
     public function paciente(){
    	return $this->belongsTo(Pacientes::class,'paciente_id');
    }
    public function caso(){
    	return $this->belongsTo(Caso::class,'caso_id');
    }
}
