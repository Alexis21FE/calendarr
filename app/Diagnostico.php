<?php

namespace App;
use App\Pacientes;
use Illuminate\Database\Eloquent\Model;
use App\Caso;
use App\Consultores;
use App\Citas;
use App\clasificacion;
class Diagnostico extends Model
{
    public function paciente(){
    	return $this->belongsTo(Pacientes::class,'paciente_id');
    }
    public function caso(){
    	return $this->hasOne(Caso::class,'diagnostico_id');
    }
    public function consultor()
    {
    	return $this->belongsTo(Consultores::Class,'consultor_id');
    }
    public function consultor_asignado(){
    	return $this->belongsTo(Consultores::class,'consultor_asig_id');
    }
    public function cita(){
        return $this->belongsTo(Citas::class,'cita_id');
    }
    public function clasificacion(){
        return $this->belongsTo(clasificacion::class,'clasificacion_id');
    }
}
