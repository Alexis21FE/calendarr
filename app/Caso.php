<?php

namespace App;
use App\Pacientes;
use App\Consultores;
use App\Diagnostico;
use App\seguimiento;
use App\familia;
use Illuminate\Database\Eloquent\Model;

class Caso extends Model
{
    public function paciente(){
    	return $this->belongsTo(Pacientes::class,'paciente_id');
    }
    public function consultor(){
    	return $this->belongsTo(Consultores::class,'consultor_id');
    }
    public function diagnostico(){
    	return $this->belongsTo(Diagnostico::class,'diagnostico_id');
    }
    public function seguimientos(){
    	return $this->hasMany(seguimiento::class,'caso_id')->orderBy('id','Desc');
    }
    public function familia(){
        return $this->hasMany(familia::class,'caso_id');
    }
     
}
