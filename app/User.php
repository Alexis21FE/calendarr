<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;
use App\Consultores;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='users';
    protected $fillable = [
        'name', 'email', 'password','role_id'
    ];
    public function role(){
       return $this->belongsTo(Role::class);
    }
    public function consultor(){
        return $this->hasOne(Consultores::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRoles(array $roles){
        //dd($this->role->name);
        foreach ($roles as $key=>$role ) {

            if($this->role->name == $role){
                
                
                return true;

            }
            # code...
        }
        return false;
    }

   
}
