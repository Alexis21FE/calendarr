<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pacientes;
use App\Consultores;
use App\Caso;
use App\Citas;
class seguimiento extends Model
{
    public function paciente(){
    	return $this->belongsTo(Pacientes::class,'paciente_id');
    }
    public function consultor(){
    	return $this->belongsTo(Consultores::class,'consultor_id');
    }
    public function caso(){
    	return $this->belongsTo(Caso::class,'caso_id');
    }
    public function cita(){
    	return $this->belongsTo(Citas::class,'cita_id');
    }
}
