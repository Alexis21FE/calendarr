<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Status;
use App\Citas;
use App\Diagnostico;
use App\seguimiento;
use App\datos_factura;
use App\sedes;
use App\Caso;
use App\familia;
use App\datos_significativos;
class Pacientes extends Model
{
	protected $table='pacientes';
    public function status(){
    	return $this->belongsTo(Status::class,'status_id');
    }
    public function factura(){
        return $this->belongsTo(datos_factura::class,'datos_factura');
    }
            public function citas()
    {
        return $this->hasMany(Citas::class,'paciente_id');
    }
    public function diagnosticos(){
    	return $this->hasOne(Diagnostico::class,'paciente_id')->orderBy('id','desc');
    }
    public function seguimientos(){
        return $this->hasMany(seguimiento::class,'paciente_id')->orderBy('id','desc');
    }
    public function colegio_s(){
        return $this->belongsTo(sedes::Class,'colegio');
    }
    public function caso(){
        return $this->hasMany(Caso::class,'paciente_id');       
    }
     public function familia(){
        return $this->hasMany(familia::class,'paciente_id');
    }
      public function datos_significativos(){
        return $this->hasMany(datos_significativos::class,'paciente_id');
    }
}
