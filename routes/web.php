<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
DB::listen(function($query){
	   /* var_dump($query->sql);
	    echo"<br>";
    var_dump($query->bindings);
	    echo"<br>";
    var_dump($query->time);
	    echo"<br>";*/
});
Route::get('emergente','PacientesController@emergente');
Route::post('users','adminController@users')->name('user');
Route::post('Consultores','HomeController@showConsultor')->name('consultor.showConsultor');
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::resource('events','CitasController',['only' => ['index','store']]);
Route::post('/events/update','CitasController@update_cita')->name('cita.update');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/dircon', 'HomeController@director_consultor')->name('home.con');
Route::get('/Consultores', 'ConsultoresController@index')->name('consultores');
Route::get('/Consultores/sh', 'HomeController@consultoreShow')->name('Consultores.show');
Route::get('Especialdades','HomeController@showEspecialidades')->name('especialidades');
Route::post('Especialdades','HomeController@showEsp_Con')->name('especialidades.post');
Route::post('/categoria','HomeController@categoria')->name('categoria');
Route::post('/becas','HomeController@becas')->name('events.becas');
Route::post('/becas/precio','HomeController@calculaBecas')->name('events.calculaBecas');
Route::post('/citas/fecha','CitasController@updateFecha')->name('update.Fecha');
Route::post('/consultorio','HomeController@checkConsultorio')->name('consultorio.libre');

Route::get('/Pacientes/Agregar','PacientesController@showFormNew')->name('paciente.new');
Route::post('/Pacientes/Agregar','PacientesController@addNewPaciente')->name('paciente.add');
Route::post('/Pacientes/Update','PacientesController@store')->name('paciente.store');
Route::get('Pacientes/Show','PacientesController@show')->name('paciente.all');
Route::get('Director/Pacientes','PacientesController@PacientesDirector')->name('director.pacientes');
Route::post('Director/Pacientes','PacientesController@PacienteDirector')->name('director.paciente');
Route::post('Pacientes/Show','PacientesController@showID')->name('paciente.Showid');
Route::get('/Consultores/Agregar','ConsultoresController@showFormNew')->name('consultores.new');
Route::post('/Consultores/Agregar','ConsultoresController@store')->name('consultores.add');
Route::post('Pacientes/Abandono','ConsultoresController@pacienteAbandono')->name('paciente.abandono');

Route::post('Reportes/calcula','recepController@calculaa')->name('reporte.diario');

//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/s', 'Auth\LoginController@login')->name('log');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        // Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

        // Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::post('password/change','UserController@changePass')->name('change.Pass');
Route::post('/price','ConsultoresController@peticion')->name('consultor.price');
/////////////////////////////////////////////
Route::get('admin/datos','adminController@datos')->name('admin.datos');


/////// Consultores
Route::get('/events/{id}','CitasController@citasConsultor');
Route::get('Reportes','recepController@corteDiario')->name('reporte.corteDiario');
Route::get('Consultores/Pacientes/{id}','ConsultoresController@showPacientes')->name('consultores.pacientes');
Route::get('Pacientes','ConsultoresController@showAll')->name('consultor.pacientes');
Route::Post('Pacientes','ConsultoresController@showPaciente')->name('consul.pacientes');
Route::Post('Pacientes/Alta','ConsultoresController@PacienteAlta')->name('paciente.alta');
Route::Post('Pacientes/Eliminar','ConsultoresController@pacienteEliminar')->name('paciente.Eliminar');
Route::Post('Pacientes/Familia','ConsultoresController@familia')->name('paciente.familiar');
Route::Post('Pacientes/Familia/delete','ConsultoresController@familia_delete')->name('paciente.familiar.delete');
Route::Post('Pacientes/Datos-Significativos','ConsultoresController@datos_significativos')->name('paciente.datos_sig');
Route::post('diagnosticos','ConsultoresController@updateDiagnostico')->name('diagnostico.guardar');
Route::post('seguimiento','ConsultoresController@addSeguimiento')->name('seguimiento.add');
Route::post('testes','ConsultoresController@testes')->name('test.paciente');
Route::get('MisDatos','ConsultoresController@datos')->name('consultor.datos');
///////////////////////////////////Configuracion///////////////////////////////////
Route::get('Configuracion/categorias','adminController@categorias')->name('configuracion.categorias');
Route::get('Configuracion/especialidades','adminController@especialidades')->name('configuracion.especialidades');
Route::get('Configuracion/becas_apoyos','adminController@becas_apoyos')->name('configuracion.becas_apoyos');
Route::get('Configuracion/Consultorios','adminController@Consultorios')->name('configuracion.Consultorios');
Route::get('Configuracion/Fondos','adminController@Fondos')->name('configuracion.fondos');
Route::get('Configuracion/Sedes','adminController@sedes')->name('configuracion.sedes');
Route::get('Configuracion/usuarios','adminController@usuarios')->name('configuracion.usuarios');
Route::post('Configuracion/usuarios','adminController@usuariosSotre')->name('usuario.store');

Route::post('Configuracion/categoria','adminController@categoriaUpdate')->name('categoria.actulizar');
Route::post('Configuracion/categoria/new','adminController@categoriaNew')->name('categoria.new');

Route::post('Configuracion/especialidad','adminController@especialidadUpdate')->name('especialidad.update');
Route::post('Configuracion/especialidad/new','adminController@especialidadNew')->name('especialidad.new');

Route::post('Configuracion/beca','adminController@becaUpdate')->name('beca.update');
Route::post('Configuracion/beca/new','adminController@becaNew')->name('beca.new');

Route::post('Configuracion/fondo','adminController@fondoUpdate')->name('fondo.abonar');
Route::post('Configuracion/fondo/new','adminController@fondoNew')->name('fondo.add');

Route::post('Configuracion/consultorio','adminController@consultorioUpdate')->name('consultorio.update');
Route::post('Configuracion/consultorio/new','adminController@consultorioNew')->name('consultorio.new');

Route::post('Configuracion/sedes','adminController@sedesUpdate')->name('sede.update');
Route::post('Configuracion/sedes/new','adminController@sedesNew')->name('sede.new');
Route::post('citaID','ConsultoresController@revisacita')->name('revisa.cita');

//Reportes
Route::get('Reportes/Detalladoporconsultor','recepController@Detalladoporconsultor')->name('reporte.Detalladoporconsultor');
Route::post('Reportes/Detalladoporconsultor','recepController@DetalladoporconsultorFecha')->name('reporte.calculaConsultor');
Route::get('Reportes/Registrodealtas','recepController@Registrodealtas')->name('reporte.Registrodealtas');
Route::post('Reportes/Registrodealtas','recepController@consultaAltas')->name('reporte.consultaAltas');
Route::get('Reportes/PacientesDetallado','recepController@PacientesDetallado')->name('reporte.PacientesDetallado');
Route::post('Reportes/PacientesDetallado','recepController@consultaPacientes')->name('reporte.consultaPacientes');
Route::get('Reportes/Detalleapoyo','recepController@Detalleapoyo')->name('reporte.Detalleapoyo');
Route::post('Reportes/Detalleapoyo','recepController@consultaApoyos')->name('reporte.consultaApoyos');
Route::get('Reportes/Beneficiospaciente','recepController@Beneficiospaciente')->name('reporte.Beneficiospaciente');
Route::post('Reportes/Beneficiospaciente','recepController@beneficiosConsulta')->name('reporte.beneficiosConsulta');
Route::get('Reportes/Casos','recepController@Casos')->name('reporte.Casos');
Route::post('Reportes/Casos','recepController@Casos_post')->name('reporte.Casos_p');
Route::get('Reportes/Diagnosticos','recepController@Diagnosticos')->name('reporte.Diagnosticos');
Route::post('Reportes/Diagnosticos','recepController@Diagnosticos_post')->name('reporte.Diagnosticos_p');
Route::get('Reportes/Citas_turno','recepController@Citas_turno')->name('reporte.Citas_turno');
Route::post('Reportes/Citas_turno','recepController@Citas_turno_post')->name('reporte.Citas_turno_p');
Route::get('Reportes/Horas_Consultor','recepController@Horas_Consultor')->name('reporte.Horas_Consultor');
Route::post('Reportes/Horas_Consultor','recepController@Horas_Consultor_post')->name('reporte.Horas_Consultor_p');
Route::get('Reportes/Seguimientos','recepController@seguimientosConsultor')->name('reporte.seguimientos');
Route::post('Reportes/Seguimientos','recepController@seguimientosConsultorPost')->name('reporte.seguimientos_p');

Route::post('Director/calendar','ConsultoresController@calendarDirector')->name('calendar.director');

Route::post('Consultor/Baja','ConsultoresController@checkBaja')->name('consultores.baja');
////////////////////////Recepcionista
Route::get('Citas/Sin_pago','recepController@citas_pendientes')->name('citas_pendientes');
Route::post('Citas/Sin_pago/id','recepController@cita_pendiente')->name('cita.response');

/////////////////////////////////
Route::get('changepass','ConsultoresController@emergente');
Route::post('pac','recepController@damepacientes')->name('dame.pacientes');
Route::get('Perfil','recepController@recepdatos')->name('recep.datos');
Route::post('Perfil','recepController@changeDatos')->name('change.datos');

Route::get('citas1','CitasController@emergente_citas');