@extends('../../layouts/app')
@section('content')
<div class="container">
    {{Form::open(['route'=>'usuario.store','method'=>'post','role'=>'form','id'=>'usuarioForm'])}}
	
<div>
		<div class="modal-dialog modal-sm" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="mySmallModalLabel">Editar usuario</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col">

							{{ Form::label('name','Nombre') }}
							{{Form::text('usuario',$user->name,['class'=>'form-control','id'=>'nombre'])}}
							{{Form::hidden('usuario_id',$user->id,['id'=>'usuario_id'])}}
						</div>
						<div class="col">

							{{ Form::label('email','Email') }}
							{{Form::text('email',$user->email,['class'=>'form-control','id'=>'email'])}} 
							@if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

						</div> 
					</div> 
					<div class="row">
						<div class="col">
							{{Form::label('activo','Activo/Desactivo')}}
							{{Form::select('activo',['A'=>'Activo','B'=>'Baja'],$user->activo,['class'=>'form-control','id'=>'activo_id','readonly'=>'readonly'])}}
						</div>
						<div class="col">
							{{Form::label('role','Rol')}}
							{{Form::select('role',$roles->pluck('display_name','id'),$user->role_id,['class'=>'form-control','id'=>'role_selec','placeholder'=>'Seleccione una opcion','readonly'=>'readonly'])}}
						</div>
					</div>
					<div class="row">
						<div class="col">
							{{Form::label('pass','Password')}}
							<input type="password" name="pass" class="form-control" id="pass">
						</div>
						<div class="col">
							{{Form::label('pass','Password')}}
							<input type="password" name="pass_" class="form-control" id="pass_">

							
						</div>
					</div>
					<div class="row">

					</div>
				</div>
				<div class="modal-footer">
					{{Form::button('cancelar',['class'=>'btn btn-danger',' data-dismiss'=>'modal'])}}
					{{Form::button('guardar',['class'=>'btn btn-success','id'=>'Guardar_'])}}
				</div>
			</div>
		</div>
	</div>
{{Form::close()}}
</div>
<script type="text/javascript">
	$("#Guardar_").click(function(){
		  var datosform=$("#usuarioForm").serialize();
		$.ajax({
			url:"{{route('usuario.store')}}",
			datatype:"json",
			type:"post",
			data:datosform,
			success:function(response){
				if(response.status!=null){
					alert(response.messagge);
				}
			},
			error:function(){}
		});
	});
</script>
@endsection