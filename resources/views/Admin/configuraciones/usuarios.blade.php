@extends('../../layouts/app')
@section('content')
<div class="container">
    {{Form::open(['route'=>'usuario.store','method'=>'post','role'=>'form','id'=>'usuarioForm'])}}
	<div id="modalEvent" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
		<div class="modal-dialog modal-sm" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="mySmallModalLabel">Editar usuario</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col">

							{{ Form::label('name','Nombre') }}
							{{Form::text('usuario',null,['class'=>'form-control','id'=>'nombre'])}}
							{{Form::hidden('usuario_id',null,['id'=>'usuario_id'])}}
						</div>
						<div class="col">

							{{ Form::label('email','Email') }}
							{{Form::text('email',null,['class'=>'form-control','id'=>'email'])}} 
							@if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

						</div> 
					</div> 
					<div class="row">
						<div class="col">
							{{Form::label('activo','Activo/Desactivo')}}
							{{Form::select('activo',['A'=>'Activo','B'=>'Baja'],'A',['class'=>'form-control','id'=>'activo_id'])}}
						</div>
						<div class="col">
							{{Form::label('role','Rol')}}
							{{Form::select('role',$roles->pluck('display_name','id'),null,['class'=>'form-control','id'=>'role_selec','placeholder'=>'Seleccione una opcion'])}}
						</div>
					</div>
					<div class="row">
						<div class="col">
							{{Form::label('pass','Password')}}
							{{Form::text('pass',null,['type'=>'password','class'=>'form-control','id'=>'pass'])}}
						</div>
						<div class="col">
							{{Form::label('pass','Password')}}
							{{Form::text('pass_',null,['type'=>'password','class'=>'form-control','id'=>'pass_'])}}
						</div>
					</div>
					<div class="row">

					</div>
				</div>
				<div class="modal-footer">
					{{Form::button('cancelar',['class'=>'btn btn-danger',' data-dismiss'=>'modal'])}}
					{{Form::button('guardar',['class'=>'btn btn-success','id'=>'Guardar_'])}}
				</div>
			</div>
		</div>
	</div>
{{Form::close()}}
	<div id="categs">

		{{Form::open(['route'=>'paciente.add','method'=>'post','role'=>'form'])}}
		@foreach($usuarios as $usuario)
		<div class="row" id="{{$usuario->id}}" style="margin-top: .8%;">
			{{Form::hidden('usuario_id',$usuario->id,['type'=>'hidden'])}}
			<div class="col" >

				{{ Form::label('name','Nombre') }}
				{{Form::text('usuario',$usuario->name,['class'=>'form-control','readonly'=>true])}}

			</div>
			<div class="col">
				{{ Form::label('email','Email') }}
				{{Form::text('email',$usuario->email,['class'=>'form-control','readonly'=>true])}} 
			</div>
			<div class="col" style="margin-top: auto;">
				{{Form::button('Editar',['class'=>'btn btn-warning', 'data-target'=>'.bd-example-modal-sm','onClick'=>'editar('.$usuario->id.')'])}}
				{{Form::button('Eliminar',['class'=>'btn btn-danger'])}}
				{{Form::button('Guardar',['class'=>'btn btn-success','onClick'=>'guardar('.$usuario->id.')'])}}
			</div>
		</div>
		@endforeach
	</div>
	<hr>

	<div class="row" id="agrega">

		<div class="col" style="text-align: end;">

			{{ Form::label('usuario','usuario') }}

		</div>

		<div class="col">
			{{Form::button('Agregar Usuario',['id'=>'agrega_usuario','class'=>'btn btn-primary','data-toggle'=>'modal', 'data-target'=>'.bd-example-modal-sm'])}}
		</div>

	</div>
	{{Form::close()}}

</div>
<script type="text/javascript">
	$(document).ready(function(){

		$("#agrega_usuario").click(function(){
			$("#modalEvent").find('input').val(null);
	});

		 $("#Guardar_").click(function(){
$pass=$("#pass").val();
	$pass_=$("#pass_").val();
	$mail=$("#mail").val();
	if($pass!=''){

	if($pass.match(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/))
    {
	if($pass==$pass_){

           var datosform=$("#usuarioForm").serialize();
		$.ajax({
			url:"{{route('usuario.store')}}",
			datatype:"json",
			type:"post",
			data:datosform,
			success:function(response){
				campo='<div class="row" id="'+response.user.id+'" style="margin-top: .8%;">'
			+'<input type="hidden" name="usuario_id" value="1">'
			+'<div class="col">'

				+'<label for="name">Nombre</label>'
				+'<input class="form-control" readonly="" name="usuario" type="text" value="'+response.user.name+'">'

			+'</div>'
			+'<div class="col">'
				+'<label for="email">Email</label>'
				+'<input class="form-control" readonly="" name="email" type="text" value="'+response.user.email+'"> '
			+'</div>'
			+'<div class="col" style="margin-top: auto;">'
			+'	<button class="btn btn-warning" data-target=".bd-example-modal-sm" onclick="editar('+response.user.id+')" type="button">Editar</button>'
				+'<button class="btn btn-danger" type="button">Eliminar</button>'
			+'	<button class="btn btn-success" onclick="guardar()" type="button">Guardar</button>'
			+'</div>'
		+'</div>';
		$("#categs").append(campo);
		$("#modalEvent").modal('hide');
			},
			error:function(){}
		});
	}else{
		alert();
	}
}else{
alert("Debe de contener 8 caracteres mayusculas y numeros ");
	}
}else{
    var datosform=$("#usuarioForm").serialize();
		$.ajax({
			url:"{{route('usuario.store')}}",
			datatype:"json",
			type:"post",
			data:datosform,
			success:function(response){	
			},
			error:function(){}
		});
}

		 });
		  });
	function editar(id){
		$.ajax({
			url:"{{route('user')}}",
			datatype:"json",
			type:"POST",
			data:{usuario_id:id},
			success:function(response){
$("#nombre").val(response.usuario.name);
$("#usuario_id").val(response.usuario.id);
$("#email").val(response.usuario.email);
$("#activo_id").val(response.usuario.activo);
$("#role_selec").val(response.usuario.role_id);
$("#modalEvent").modal();
			},
			error:function(){}
		});
	}
	

</script>
@endsection