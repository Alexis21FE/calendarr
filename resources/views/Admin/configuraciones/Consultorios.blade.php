@extends('../../layouts/app')
@section('content')
<div class="container">
  <div id="categs">
    @foreach($consultorios as $consultorio)
    <div class="row" id="{{$consultorio->id}}" style="margin-top: .8%;">
      {{Form::hidden('consultorio_id',$consultorio->id,['type'=>'hidden'])}}
      <div class="col" style="text-align: end;">

        {{ Form::label('consultorio','consultorio') }}

      </div>
      <div class="col">
       {{Form::text('consultorio',$consultorio->descripcion,['class'=>'form-control','readonly'=>true])}}
     </div>
     <div class="col">
      {{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar('.$consultorio->id.')'])}}
      {{Form::button('Eliminar',['class'=>'btn btn-danger'])}}
      {{Form::button('Guardar',['class'=>'btn btn-success','onClick'=>'guardar('.$consultorio->id.')'])}}
    </div>
  </div>
  @endforeach
</div>
<hr>

<div class="row" id="agrega">

 <div class="col" style="text-align: end;">

  {{ Form::label('consultorio','consultorio') }}

</div>
<div class="col">
 {{Form::text('consultorio',null,['class'=>'form-control'])}}
</div>

<div class="col">
  {{Form::button('Agregar',['class'=>'form-control btn btn-primary','id'=>'agrega_consultorio'])}}
</div>

</div>

</div>
<script type="text/javascript">
  $(document).ready(function(){

   $("#agrega_consultorio").click(function(){
    var form=$("#agrega").find("input").serialize();
    $.ajax({
      url:"{{route('consultorio.new')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
          $("#agregar").find("input").attr("val","");
          campo='<div class="row" id="'+response.consultorio.id+'" style="margin-top: .8%;">'+
          '<input type="hidden" name="consultorio_id" value="'+response.consultorio.id+'">'+
          '<div class="col" style="text-align: end;">'+
          '<label for="consultorio">consultorio</label>'+

          '</div>'+
          '<div class="col">'+
          '<input class="form-control" name="consultorio" type="text" value="'+response.consultorio.descripcion+'" id="consultorio" readonly="readonly">'+
          '</div>'+
          '<div class="col">'+
          '<button class="btn btn-warning" onclick="editar('+response.consultorio.id+')" type="button">Editar</button>'+
          '<button class="btn btn-danger" type="button">Eliminar</button>'+
          ' <button class="btn btn-success" onclick="guardar('+response.consultorio.id+')" type="button">Guardar</button>'+
          '</div>'+
          '</div>';
          $("#categs").append(campo);
          $("#agrega").find("input").val(" ");
        }else{
          alert(response.mensaje);
        }
      },
      error:function(){}
    });
  });


 });
  function editar(id){
    $("#"+id).find('input').prop('readonly',false);
  }
  function guardar(id){
    var consultorio_id=id;
    var form=$("#"+id).find("input").serialize()
    console.log(form);
    $.ajax({
      url:"{{route('consultorio.update')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
         $("#"+id).find("input").prop('readonly',true);
         alert(response.mensaje);
       }
     },
     error:function(){

     }
   });
  }

</script>
  @endsection