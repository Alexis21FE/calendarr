@extends('../../layouts/app')
@section('content')
<div class="container">
  <div id="categs">

    {{Form::open(['route'=>'paciente.add','method'=>'post','role'=>'form'])}}
    @foreach($especialidades as $especialidad)
    <div class="row" id="{{$especialidad->id}}" style="margin-top: .8%;">
      {{Form::hidden('especialidad_id',$especialidad->id,['type'=>'hidden'])}}
      <div class="col" style="text-align: end;">

        {{ Form::label('especialidad','Especialidad') }}

      </div>
      <div class="col">
       {{Form::text('especialidad',$especialidad->descripcion,['class'=>'form-control','readonly'=>true])}}
     </div>
     <div class="col">
      {{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar('.$especialidad->id.')'])}}
      {{Form::button('Eliminar',['class'=>'btn btn-danger'])}}
      {{Form::button('Guardar',['class'=>'btn btn-success','onClick'=>'guardar('.$especialidad->id.')'])}}
    </div>
  </div>
  @endforeach
</div>
<hr>

<div class="row" id="agrega">

 <div class="col" style="text-align: end;">

  {{ Form::label('especialidad','Especialidad') }}

</div>
<div class="col">
 {{Form::text('especialidad',null,['class'=>'form-control'])}}
</div>

<div class="col">
  {{Form::button('Agregar',['class'=>'form-control btn btn-primary','id'=>'agrega_especialidad'])}}
</div>

</div>
{{Form::close()}}

</div>
<script type="text/javascript">
  $(document).ready(function(){

   $("#agrega_especialidad").click(function(){
    var form=$("#agrega").find("input").serialize();
    $.ajax({
      url:"{{route('especialidad.new')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
          $("#agregar").find("input").attr("val","");
          campo='<div class="row" id="'+response.especialidad.id+'" style="margin-top: .8%;">'+
          '<input type="hidden" name="especialidad_id" value="'+response.especialidad.id+'">'+
          '<div class="col" style="text-align: end;">'+
          '<label for="especialidad">Especialidad</label>'+

          '</div>'+
          '<div class="col">'+
          '<input class="form-control" name="especialidad" type="text" value="'+response.especialidad.descripcion+'" id="especialidad">'+
          '</div>'+
          '<div class="col">'+
          '<button class="btn btn-warning" onclick="editar('+response.especialidad.id+')" type="button">Editar</button>'+
          '<button class="btn btn-danger" type="button">Eliminar</button>'+
          ' <button class="btn btn-success" onclick="guardar('+response.especialidad.id+')" type="button">Guardar</button>'+
          '</div>'+
          '</div>';
          $("#categs").append(campo);
        }else{
          alert(response.mensaje);
        }
      },
      error:function(){}
    });
  });


 });
  function editar(id){
    $("#"+id).find('input').prop('readonly',false);
  }
  function guardar(id){
    var especialidad_id=id;
    var form=$("#"+id).find("input").serialize()
    console.log(form);
    $.ajax({
      url:"{{route('especialidad.update')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
         $("#"+id).find("input").prop('readonly',true);
         alert(response.mensaje);
       }
     },
     error:function(){

     }
   });
  }

</script>
@endsection