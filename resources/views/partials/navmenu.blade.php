    @php
    $con_si=Session::get('con_si',false);
    
    @endphp
    @if(auth()->user()->hasRoles(['recep','consul','admin']))
    <nav class="navbar navbar-expand-lg navbar-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/home') }}">
        <img src="{{url('/img/logo.png')}}" height="50" alt="">
      </a>

      <div class="collapse navbar-collapse bg-primary" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}"><i class="fas fa-calendar-alt"></i>Calendario</a></li>
          @if(auth()->user()->hasRoles(['direc','recep','consul']))
         <li class="nav-item dropdown ">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Pacientes<span class="caret"></span>
          </a>
          @if(auth()->user()->hasRoles(['direc','recep']))
          
          <div class="dropdown-menu " aria-labelledby="navbarDropdown">
            <a class="nav-link con_no" href="{{route('paciente.new')}}">Agregar Paciente<span class="sr-only">(current)</span></a>  
            <a class="nav-link con_no" href="{{route('paciente.all')}}">Ver pacientes<span class="sr-only">(current)</span></a>
            <a class="nav-link con_si" style="{{($con_si==true)?'display:block;':'display: none;'}}" href="{{route('consultor.pacientes')}}">Pacientes<span class="sr-only">(current)</span></a>
          </div>
          @endif
           @if(auth()->user()->hasRoles(['consul']))
          <div class="dropdown-menu con_si" aria-labelledby="navbarDropdown">
            <a class="nav-link" href="{{route('consultor.pacientes')}}">Pacientes<span class="sr-only">(current)</span></a>
          </div>
          @endif
        </li>
        @endif
        @if(auth()->user()->hasRoles(['direc','admin']))
        <li class="nav-item dropdown con_no">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Consultores<span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('Consultores.show')}}">
              {{ __('Ver Consultores') }}
            </a>
            <a class="dropdown-item" href="{{ route('consultores.new') }}">
              {{ __('Agregar Consultor') }}
            </a>
          </div>
        </li>
        @endif

          @if(auth()->user()->hasRoles(['direc','recep']))
      <li class="con_no"><a class="nav-link" href="{{ route('especialidades') }}">{{ __('Consultor Especialidades') }}</a></li>
          @endif
        @if(auth()->user()->hasRoles(['admin','direc','recep','consul']))
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Reportes<span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('reporte.corteDiario')}}">
              {{ __('Corte Diario') }}
            </a>
               @if(auth()->user()->hasRoles(['admin','direc','recep'])) 
               <div class="con_no">
                 
            <a class="dropdown-item" href="{{route('reporte.Detalladoporconsultor')}}">
              {{ __('Detallado por consultor') }}
            </a>     
            <a class="dropdown-item" href="{{route('reporte.Registrodealtas')}}">
              {{ __('Registro de altas') }}
            </a>     
            <a class="dropdown-item" href="{{route('reporte.PacientesDetallado')}}">
              {{ __('Pacientes (Detallado)') }}
            </a>     
            <a class="dropdown-item" href="{{route('reporte.Detalleapoyo')}}">
              {{ __('Detalle apoyos') }}
            </a>      
            <a class="dropdown-item" href="{{route('reporte.Beneficiospaciente')}}">
              {{ __('Beneficios paciente') }}
            </a>
         
           
            <a class="dropdown-item" href="{{route('reporte.Citas_turno')}}">
              {{ __('Citas por turno') }}
            </a>
            <a class="dropdown-item" href="{{route('reporte.Horas_Consultor')}}">
              {{ __('Horas por consultor' ) }}
            </a>

               </div>     
            @endif
               <a class="dropdown-item" href="{{route('reporte.Casos')}}">
              {{ __('Casos') }}
            </a> 
             <a class="dropdown-item" href="{{route('reporte.seguimientos')}}">
              {{ __('Seguimientos Consultor' ) }}
            </a>
          </div>
        </li>
        @endif
        @if(auth()->user()->hasRoles(['direc','admin']))
        <li class="nav-item dropdown con_no">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Configuracion<span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('configuracion.categorias')}}">
              {{ __('Categorias') }}
            </a>
            <a class="dropdown-item" href="{{route('configuracion.especialidades')}}">
              {{ __('Especialidades') }}
            </a>       
            <a class="dropdown-item" href="{{route('configuracion.becas_apoyos')}}">
              {{ __('Becas y Apoyos') }}
            </a>      
            <a class="dropdown-item" href="{{route('configuracion.Consultorios')}}">
              {{ __('Consultorios') }}
            </a>  
            <a class="dropdown-item" href="{{route('configuracion.fondos')}}">
              {{ __('Fondos') }}
            </a>     
              <a class="dropdown-item" href="{{route('configuracion.usuarios')}}">
              {{ __('Control de Usuarios') }}
            </a>
             <a class="dropdown-item" href="{{route('configuracion.sedes')}}">
              {{ __('Sedes') }}
            </a>
                <a class="dropdown-item" href="{{route('reporte.seguimientos')}}">
              {{ __('Seguimientos Consultor' ) }}
            </a>
          </div>
        </li>
        @endif
        <!-- Authentication Links -->
        @guest
        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
        @else
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            @if(auth()->user()->hasRoles(['consul']))
               <a class="dropdown-item" href="{{route('consultor.datos')}}">
              {{ __('Mis datos') }}
            </a>
            @endif
            <a class="dropdown-item con_si" style="display: none;" href="{{route('consultor.datos')}}">
              {{ __('Mis datos') }}
            </a>
             @if(auth()->user()->hasRoles(['admin']))
               <a class="dropdown-item" href="{{route('admin.datos')}}">
              {{ __('Mis datos') }}
            </a>
            @endif
             @if(auth()->user()->hasRoles(['recep']))
               <a class="dropdown-item" href="{{route('recep.datos')}}">
              {{ __('Mis datos') }}
            </a>
            @endif
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
      @if(auth()->user()->hasRoles(['direc','recep']))
      <!--li><a class="nav-link" href="{{ route('citas_pendientes') }}">{{ __('Citas sin pago') }}</a></li-->
      @endif

      @endguest
    
    @if(auth()->user()->hasRoles(['direc']))
  <div style="   
    text-align: right;
    line-height: 1.6;
    max-width: 15%;" >
    
    <li class="nav-item">
      <span class="navbar-text" id="label_dis">
    Director
  </span>
    </li>
        </div>
    <li style="margin: auto;">
      
 <input type="radio" name="interruptor" id="prendido">
  <input type="radio" name="interruptor" id="apagado" checked>

  <div class="interruptor-cuerpo" >
      <div class="interruptor-tecla">
          <label class="label_dis" for="prendido" title="Desactivado">___</label>
          <label class="label_dis" for="apagado" title="Activado">___</label>
      </div>    
  </div>
    </li>

  @endif
  </ul>
  </div>
</nav>
@elseif(auth()->user()->hasRoles(['direc']))
@if( $con_si==true)
<nav class="navbar navbar-expand-lg navbar-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/home') }}">
        <img src="{{url('/img/logo.png')}}" height="50" alt="">
      </a>

      <div class="collapse navbar-collapse bg-primary" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
           <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}"><i class="fas fa-calendar-alt"></i>Calendario</a></li>
          @if(auth()->user()->hasRoles(['direc','recep','consul']))
         <li class="nav-item dropdown ">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Pacientes<span class="caret"></span>
          </a>
          <div class="dropdown-menu " aria-labelledby="navbarDropdown">
            <a class="nav-link con_si"  href="{{route('consultor.pacientes')}}">Pacientes<span class="sr-only">(current)</span></a>
          </div>
         
        </li>
        @endif

     
        @if(auth()->user()->hasRoles(['admin','direc','recep','consul']))
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Reportes<span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('reporte.corteDiario')}}">
              {{ __('Corte Diario') }}
            </a>
                   <a class="dropdown-item" href="{{route('reporte.Casos')}}">
              {{ __('Casos') }}
            </a> 
                  <a class="dropdown-item" href="{{route('reporte.seguimientos')}}">
              {{ __('Seguimientos Consultor' ) }}
            </a>
          </div>
        </li>
        @endif
  
        <!-- Authentication Links -->
        @guest
        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
        @else
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        
               <a class="dropdown-item" href="{{route('consultor.datos')}}">
              {{ __('Mis datos') }}
            </a>
     
            <a class="dropdown-item con_si" style="display: none;" href="{{route('consultor.datos')}}">
              {{ __('Mis datos') }}
            </a>
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>


      @endguest
    
    @if(auth()->user()->hasRoles(['direc']))
  <div style="   
    text-align: right;
    line-height: 1.6;
    max-width: 15%;" >
    
    <li class="nav-item">
      <span class="navbar-text" id="label_dis">
    Consultor
  </span>
    </li>
        </div>
    <li style="margin: auto;">
      
 <input type="radio" name="interruptor" id="prendido" checked>
  <input type="radio" name="interruptor" id="apagado" >

  <div class="interruptor-cuerpo" >
      <div class="interruptor-tecla">
          <label class="label_dis" for="prendido" title="Desactivado">___</label>
          <label class="label_dis" for="apagado" title="Activado">___</label>
      </div>    
  </div>
    </li>

  @endif
  </ul>
  </div>
</nav>
@else
<nav class="navbar navbar-expand-lg navbar-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/home') }}">
        <img src="{{url('/img/logo.png')}}" height="50" alt="">
      </a>

      <div class="collapse navbar-collapse bg-primary" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
           <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}"><i class="fas fa-calendar-alt"></i>Calendario</a></li>
          @if(auth()->user()->hasRoles(['direc','recep','consul']))
         <li class="nav-item dropdown ">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Pacientes<span class="caret"></span>
          </a>
          @if(auth()->user()->hasRoles(['direc','recep']))
          
          <div class="dropdown-menu " aria-labelledby="navbarDropdown">
            <a class="nav-link con_no" href="{{route('paciente.new')}}">Agregar Paciente<span class="sr-only">(current)</span></a>  
            <a class="nav-link con_no" href="{{route('director.pacientes')}}">Ver pacientes<span class="sr-only">(current)</span></a>
            <a class="nav-link con_si" style="{{($con_si==true)?'display:block;':'display: none;'}}" href="{{route('consultor.pacientes')}}">Pacientes<span class="sr-only">(current)</span></a>
          </div>
          @endif
           @if(auth()->user()->hasRoles(['consul']))
          <div class="dropdown-menu con_si" aria-labelledby="navbarDropdown">
            <a class="nav-link" href="{{route('consultor.pacientes')}}">Pacientes<span class="sr-only">(current)</span></a>
          </div>
          @endif
        </li>
        @endif
        @if(auth()->user()->hasRoles(['direc']))
        <li class="nav-item dropdown con_no">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Consultores<span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('Consultores.show')}}">
              {{ __('Ver Consultores') }}
            </a>
            <a class="dropdown-item" href="{{ route('consultores.new') }}">
              {{ __('Agregar Consultor') }}
            </a>
          </div>
        </li>
        @endif

          @if(auth()->user()->hasRoles(['direc','recep']))
      <li class="con_no"><a class="nav-link" href="{{ route('especialidades') }}">{{ __('Consultor Especialidades') }}</a></li>
          @endif
        @if(auth()->user()->hasRoles(['admin','direc','recep','consul']))
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Reportes<span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('reporte.corteDiario')}}">
              {{ __('Corte Diario') }}
            </a>
               @if(auth()->user()->hasRoles(['admin','direc','recep'])) 
               <div class="con_no">
                 
            <a class="dropdown-item" href="{{route('reporte.Detalladoporconsultor')}}">
              {{ __('Detallado por consultor') }}
            </a>     
            <a class="dropdown-item" href="{{route('reporte.Registrodealtas')}}">
              {{ __('Registro de altas') }}
            </a>     
            <a class="dropdown-item" href="{{route('reporte.PacientesDetallado')}}">
              {{ __('Pacientes (Detallado)') }}
            </a>     
            <a class="dropdown-item" href="{{route('reporte.Detalleapoyo')}}">
              {{ __('Detalle apoyos') }}
            </a>      
            <a class="dropdown-item" href="{{route('reporte.Beneficiospaciente')}}">
              {{ __('Beneficios paciente') }}
            </a>
            <a class="dropdown-item" href="{{route('reporte.Casos')}}">
              {{ __('Casos') }}
            </a> 
            <a class="dropdown-item" href="{{route('reporte.Citas_turno')}}">
              {{ __('Citas por turno') }}
            </a>
            <a class="dropdown-item" href="{{route('reporte.Horas_Consultor')}}">
              {{ __('Horas por consultor' ) }}
            </a>
                <a class="dropdown-item" href="{{route('reporte.seguimientos')}}">
              {{ __('Seguimientos Consultor' ) }}
            </a>
               </div>     
            @endif
          </div>
        </li>
        @endif
        @if(auth()->user()->hasRoles(['direc','admin']))
        <li class="nav-item dropdown con_no">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Configuracion<span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('configuracion.categorias')}}">
              {{ __('Categorias') }}
            </a>
            <a class="dropdown-item" href="{{route('configuracion.especialidades')}}">
              {{ __('Especialidades') }}
            </a>       
            <a class="dropdown-item" href="{{route('configuracion.becas_apoyos')}}">
              {{ __('Becas y Apoyos') }}
            </a>      
            <a class="dropdown-item" href="{{route('configuracion.Consultorios')}}">
              {{ __('Consultorios') }}
            </a>  
            <a class="dropdown-item" href="{{route('configuracion.fondos')}}">
              {{ __('Fondos') }}
            </a>     
              <a class="dropdown-item" href="{{route('configuracion.usuarios')}}">
              {{ __('Control de Usuarios') }}
            </a> 
            <a class="dropdown-item" href="{{route('configuracion.sedes')}}">
              {{ __('Sedes') }}
            </a>
          </div>
        </li>
        @endif
        <!-- Authentication Links -->
        @guest
        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
        @else
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            @if(auth()->user()->hasRoles(['consul']))
               <a class="dropdown-item" href="{{route('consultor.datos')}}">
              {{ __('Mis datos') }}
            </a>
            @endif
            <a class="dropdown-item con_si" style="display: none;" href="{{route('consultor.datos')}}">
              {{ __('Mis datos') }}
            </a>
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>


      @endguest
    
    @if(auth()->user()->hasRoles(['direc']))
  <div style="   
    text-align: right;
    line-height: 1.6;
    max-width: 15%;" >
    
    <li class="nav-item">
      <span class="navbar-text" id="label_dis">
    Director
  </span>
    </li>
        </div>
    <li style="margin: auto;">
      
 <input type="radio" name="interruptor" id="prendido">
  <input type="radio" name="interruptor" id="apagado" checked>

  <div class="interruptor-cuerpo" >
      <div class="interruptor-tecla">
          <label class="label_dis" for="prendido" title="Desactivado">___</label>
          <label class="label_dis" for="apagado" title="Activado">___</label>
      </div>    
  </div>
    </li>

  @endif
  </ul>
  </div>
</nav>
@endif
@endif

