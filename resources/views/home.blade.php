    
    @extends('layouts.app')
    @section('content')
    @if(auth()->user()->hasRoles(['admin','direc','recep']))

    
    
    <body>
      <div id="container">

        <div id="calendario_size"> 
          {{Form::open(['route'=>'events.store','method'=>'post','role'=>'form','id'=>'miFormulario'])}}
          <div id="responsive-modal" class="modal" tabindex="-1" data-backdrop="static" >
            <div class="modal-dialog">
              <div class="modal-content">

                <div class="modal-header">
                  <h4>Registro de una nueva cita</h4>

                </div>
                <div class="modal-body">
                  <div class="row">

                    <div class="col">

                      {{ Form::label('consultor','consultor') }}
                      <select id ='consultor'   class="form-control" name="consultor" required="true" >
                        <option value="" disabled selected hidden>Elija un consultor</option>
                        @foreach($consultores as $consultor)
                        <option value="{{$consultor->id}}">{{$consultor->nombre." ".$consultor->apellido_pa." ".$consultor->apellido_ma }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col">
                      {{ Form::label('paciente','Paciente') }}
                      <select id ='paciente'   class="form-control" name="paciente" required="true">
                        <option value="" disabled selected hidden>Elija un paciente</option>
                        @foreach($pacientes as $paciente)
                        <option value="{{$paciente->id}}">{{$paciente->nombre." ".$paciente->apellido_pa." ".$paciente->apellido_ma." - Pac ".$paciente->nombreconsultor }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="col">
                      {{ Form::label('tipo_cita', 'Tipo de Cita') }}
                      {{ Form::select('tipo_cita', $tipo_citas, null, ['id' => 'tipo_cita','class'=>'form-control','required'=>true,'placeholder'=>'Seleccione un tipo de cita']) }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">

                    <div class="col">
                      {{ Form::label('date_start','Fecha Cita') }}
                      {{Form::text('date_start',old('date_start'),['class'=>'form-control', 'readonly'=>'true'])}}
                    </div>
                    <div class="col">
                      {{Form::label('time_start','Hora inicio')}}
                      {{Form::time('time_start',null,['class'=>'form-control'])}}

                    </div>
                    <div class="col">
                      {{Form::label('date_end','Hora fin')}}
                      {{Form::time('date_end',null,['class'=>'form-control'])}}
                    </div>

                  </div>
                  <div class="row">

                    <div class="col">
                     {{ Form::label('consultorio','Consultorios') }}
                     {{ Form::select('consultorio', $consultorios, null, ['id' => 'consultorios',    'class'=>'form-control multiple ','required'=>'required','placeholder'=>'Elija un consultorio']) }}
                   </div>
                   <div class="col">
                    {{$errors->first('individual_pareja')}}
                    {{ Form::label('ind_par','Individual Pareja/Familiar') }}
                    {{Form::select('individual_pareja', ['Individual' => 'Individual', 'Pareja' => 'Pareja','Familiar'=>'Familiar'],null,['class'=>'form-control','id'=>'ind_par','placeholder'=>'Elija una opcion de cita','required'=>true])}}
                  </div>
                  <div class="col">
                   {{ Form::label('costo','Costo Total') }}
                   {{ Form::text('costo',null,['id' => 'costo','class'=>'form-control','readonly'=>'readonly']) }}
                 </div>

               </div>
               <hr>
               <div class="row">

                <div class="col">
                 {{ Form::label('becas','Becas') }}
                 {{ Form::select('becas_descuentos', $becas_descuentos->pluck('descripcion','id'), null, ['id' => 'beca_descuento',    'class'=>'form-control ','placeholder' => 'Elija una beca/descuento']) }}
               </div>
               <div class="col">
                {{ Form::label('becas','Porcentaje o Descuento') }}
                {{Form::text('porcentaje',old('porcentaje'),['class'=>'form-control','id' => 'beca_porcentaje' ])}}

              </div>
              <div class="col" style="margin: auto; margin-top: 3.5%; text-align: center;">
                {!! Html::decode(Form::button('<i class="fas fa-plus-circle "></i>Agregar Beca',['class' => 'btn btn-success','id'=>'agregarBeca'])) !!}
              </div>
            </div>
            <hr>

            <div id="Becas_agregadas">
              {{Form::hidden('costo__',null,['id'=>'costo__','type'=>'hidden'])}}
              @for($x=1;$x<=$becas_descuentos->max('orden');$x++)
              <div id="orden{{$x}}"></div>
              @endfor


            </div>

            <div class="row">
              <div class="col">
               {{ Form::label('costo','Costo Final') }}
               {{ Form::text('costo_final',null,['id' => 'costo_final','class'=>'form-control','readonly'=>'readonly']) }}
             </div>
             <div class="col">
              {{Form::label('observaciones','Observaciones')}}
              {{Form::text('observaciones',null,['id'=>'observaciones','class'=>'form-control'])}}
             </div>
             <div class="col">

             </div>

           </div>                                                 

         </div>
         <div class="modal-footer">
          <button type="button" class="btn btn-danger" id="close_modal" data-dismiss="modal">Cancelar</button>
          {{Form::submit('Guardar',['class'=>'btn btn-primary','id'=>'btnproob'])}}
        </div>
      </div>
    </div>
  </div>
  {{Form::close()}}


  <div id='calendar'></div>
  {{Form::open(['route'=>'cita.update','method'=>'post','role'=>'form'])}}
  {{Form::hidden('cita_id',null,['type'=>'hidden','id'=>'cita_id'])}}
  <div id="modalEvent" class="modal" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4>Detalle de la cita</h4>

        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col">

              {{ Form::label('consultor','consultor') }}
              <select id ='consultor_'   class="form-control" name="consultor" required="true" >
                <option value="" disabled selected hidden>Elija un consultor</option>
                @foreach($consultores as $consultor)
                <option value="{{$consultor->id}}">{{$consultor->nombre." ".$consultor->apellido_pa." ".$consultor->apellido_ma }}</option>
                @endforeach
              </select>
            </div>
            <div class="col">
              {{ Form::label('paciente_','Paciente') }}
              <select id ='paciente_'   class="form-control" name="paciente" required="true">
                        <option value="" disabled selected hidden>Elija un paciente</option>
                        @foreach($pacientes as $paciente)
                        <option value="{{$paciente->id}}">{{$paciente->nombre." ".$paciente->apellido_pa." ".$paciente->apellido_ma }}</option>
                        @endforeach
                      </select>
            </div>

            <div class="col">
              {{ Form::label('tipo_cita_', 'Tipo de Cita') }}
              {{ Form::select('tipo_cita_', $tipo_citas, null, ['id' => 'tipo_cita_','class'=>'form-control multiple ']) }}
            </div>
          </div>
          <div class="row">
            <div class="col">
              {{ Form::label('date_start_','Fecha Cita') }}
              {{Form::text('date_start_',old('date_start_'),['class'=>'form-control','type'=>'date'])}}
            </div>

            <div class="col">
              {{Form::label('time_start_','Hora inicio')}}
              {{Form::time('time_start_',null,['class'=>'form-control'])}}

            </div>
            <div class="col">
              {{Form::label('date_end_','Hora fin')}}
              {{Form::time('date_end_',null,['class'=>'form-control'])}}
            </div>

          </div>
          <div class="row">
            <div class="col">
             {{ Form::label('consultorio_','Consultorios') }}
             {{ Form::select('consultorio_', $consultorios, null, ['id' => 'consultorio_',    'class'=>'form-control multiple ']) }}
           </div>
           <div class="col">
            {{ Form::label('ind_par','Individual Pareja/Familiar') }}
            {{Form::select('individual_pareja_',  ['Individual' => 'Individual', 'Pareja' => 'Pareja','Familiar'=>'Familiar'],null,['class'=>'form-control','placeholder' => 'Elija una opcion','id'=>'individual_pareja_'])}}
          </div>
          <div class="col">
            {{ Form::label('status_cita_','Status Cita') }}
            {{ Form::select('status_cita_', $status_cita, null, ['id' => 'status_cita_',    'class'=>'form-control multiple ']) }}
            {{Form::label('texto_pagada','Fecha de pago ',['style'=>'display:none;','id'=>'text_cita_pagada'])}}
          </div>

        </div>
        <div class="row">

          <div class="col">
           {{ Form::label('becas','Becas') }}
           {{ Form::select('becas_descuentos', $becas_descuentos->pluck('descripcion','id'), null, ['id' => 'beca_descuento_',    'class'=>'form-control ','placeholder' => 'Elija una beca/descuento']) }}
         </div>
         <div class="col">
          {{ Form::label('becas','Porcentaje o Descuento') }}
          {{Form::text('porcentaje',old('porcentaje'),['class'=>'form-control','id' => 'beca_porcentaje_' ])}}

        </div>
        <div class="col" style="margin: auto; margin-top: 3.5%; text-align: center;">
          {!! Html::decode(Form::button('<i class="fas fa-plus-circle "></i>Agregar Beca',['class' => 'btn btn-success','id'=>'agregarBeca_'])) !!}
        </div>
      </div>
      <hr>
      <div id="Becas_agregadas_">
        {{Form::hidden('costo__',null,['type'=>'hidden','id'=>'costo_ini'])}}
        @for($x=1;$x<=$becas_descuentos->max('orden');$x++)
        <div id="orden_{{$x}}"></div>
        @endfor


      </div>
      <div class="row">
        <div class="col">
         {{ Form::label('costo_','Costo Total') }}
         {{ Form::text('costo_final',null,['id' => 'costo_final_','class'=>'form-control','readonly'=>'readonly']) }}
       </div>
       <div class="col">
         {{Form::label('observaciones','Observaciones')}}
         {{Form::text('observaciones',null,['id'=>'observaciones','class'=>'form-control'])}}
       </div>

     </div>         


   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-danger" id="close_modal" data-dismiss="modal">Cancelar</button>
    {{Form::submit('Guardar',['class'=>'btn btn-primary','id'=>'updateCita'])}}
  </div>
</div>
</div>
</div>
{{Form::close()}}
</div>
</div>
</body>




<script>

  $(document).ready(function(){

    $("#updateCita").click(function(e){

      var pagada = $("#status_cita_").children(':selected').text();
      var fecha_pago="{{date('d/m/Y H:m')}}"
      if(pagada=='pagada'){
       if (!confirm("Estas seguro actulizar la cita con fecha de pago "+fecha_pago)) {
         e.preventDefault()
         revertFunc();
       }else{

        $("#updateCita").submit();
      }
    }else{
      $("#updateCita").submit();
    }
  });
    $("#close_modal").click(function(){
       $('#miFormulario').trigger("reset");
    });
   
    var nextinput=0;
    porcentaje='';
    $("#close_modal").modal("hide");
    var baseurl='{{url('/')}}';
     console.log(baseurl+'/events');

    $('#calendar').fullCalendar({
      locale:'es',
      themeSystem:'bootstrap4',
      nowIndicator:false,
      slotLabelFormat:'h:mm:a',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
         events: baseurl+'/events', 
      defaultDate: '{{date('Y-m-d')}}',
                          navLinks: true, // can click day/week names to navigate views
                          editable: true,
                          defaultView: 'agendaWeek',
                          minTime:'07:00:00',
                          maxTime:'23:00:00',
                          allDayText:'Horario',
                          selectable:true,
                          selectHelper:true,
                          eventRender: function(event, element)
                          { 
                            var nombre;
                            if(event.status_cita.descripcion=='confirmada'){
                             element.find('.fc-time').prepend("<span><i class='fas fa-check-circle'></i></span>");
                           }
                           if(event.status_cita.descripcion=='Cancelada'){
                            element.find('.fc-content').css('text-decoration','line-through');
                          } 
                            
                         
                                        
                                     element.find('.fc-content').append("<div class='fc-time'><span>"+event.consultor.nombrecompleto+"</span></div>");
                                        /* element.find('.fc-content').append("<div class='fc-time'><span>"+event.paciente.nombrecompleto+"</span></div>");
*/
                          element.find('.fc-content').append("<span>"+event.status_cita.descripcion+" "+event.monto_final+"</span>");

                        },

                        eventDrop: function(event, delta, revertFunc) {
                         if (!confirm("Estas seguro de reagendar la cita?")) {
                          revertFunc();
                        }else{
                          fecha_nueva=event.start.format('YYYY-MM-DD');
                          hora_inicio_nueva=event.start.format('HH:mm:ss');
                          hora_fin_nueva=event.end.format('HH:mm:ss');
                          cita_id=event.id;
                          $.ajax({
                            url:"{{route('update.Fecha')}}",
                            data:{
                              fecha_nueva:fecha_nueva,
                              hora_inicio_nueva:hora_inicio_nueva,
                              hora_fin_nueva:hora_fin_nueva,
                              cita_id:cita_id
                            },dataType:'json',
                            type:'post',
                            success: function(response){
                              if(response.status==true){
                                swal(response.mensaje);
                              }else{
                                swal(response.mensaje);
                              }
                            },
                            error:function(){

                            }

                          });

                        }
                      },
                      select:function(start){
                        start=moment(start.format());
                        $("#date_start").val(start.format('YYYY-MM-DD'));
                        $("#time_start").val(start.format('HH:mm'));
                        $("#date_end").val(start.format('HH:mm:ss'));
                        var dateend=$("#date_end").val();

                        dateend=parseFloat(dateend)+parseFloat(1);
                        $("#date_end").val(dateend+':00');
                        $("#responsive-modal").modal('show');
                      },
                   
                      eventClick:function(event,jsEvent,view){
                        var date_start = $.fullCalendar.moment(event.start).format('YYYY-MM-DD');
                        var time_start = $.fullCalendar.moment(event.start).format('HH:mm');
                        var date_end = $.fullCalendar.moment(event.end).format('HH:mm');
                        $("#modalEvent #title").val(event.descripcion);
                        $("#modalEvent #date_start_").val(date_start);
                        $("#modalEvent #time_start_").val(time_start);
                        $("#modalEvent #date_end_").val(date_end);
                        $("#consultor_").val(event.consultor_id);
                        $("#paciente_").val(event.paciente.id); 
                        $("#cita_id").val(event.id);
                    $.ajax({
      url:"{{route('consultor.price')}}",
      data:{
        consultor_id:event.consultor_id,
        tipo_cita:"seguimiento",
        fecha:date_start,
        start:time_start,
        end:date_end 
      },
      dataType:"json",
      method:"Post",
      success:function(response){
          $("#paciente_").val(event.paciente.id);
        $("#paciente_").empty();

            $(response.pacientes).each(function(index,pac){

              if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }
          $("#paciente_").append('<option style="background:'+response.consultor.color+'" value="'+ pac.id +'">'+ pac.nombrecompleto+'  - Pac '+pac.nombreconsultor+'</option>');
        });
        $(response.pacientes_).each(function(index,pac){
           if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }
          $("#paciente_").append('<option value="'+ pac.id +'">'+ pac.nombrecompleto+'</option>');
   
        });
          $(response.pacientes_dif).each(function(index,pac){
            if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }
          $("#paciente_").append('<option value="'+ pac.id +'">'+ pac.nombrecompleto+'  - Pac '+pac.nombreconsultor+'</option>');
        });
          $("#paciente_").val(event.paciente.id); 
        if(response.status==true){  
          $("#costo").val(response.costo);
          $("#costo_final").val(response.costo);
         
        }else{
          $("#costo").val(0);
        }
      },fail:function(){

      }

    });
                        $("#tipo_cita_").val(event.tipo_cita_id);
                        $("#consultorio_").val(event.consultorio_id);
                        $("#individual_pareja_").val(event.individual_pareja);
                        $("#costo_final_").val(event.monto_final);
                        $("#costo_ini").val(event.monto_inicial);
                      
                        $("#Becas_agregadas_").children('div').empty();
                        if(event.status_cita.descripcion=='pagada'){
                          $("#modalEvent").find('input').attr('readonly',true);
                          $("#modalEvent").find('select').attr('disabled',true);
                          $("#text_cita_pagada").css('display','block');
                          $("#text_cita_pagada").append(event.fecha_pago);
                        }else{
                          $("#modalEvent").find('input').attr('readonly',false);
                          $("#modalEvent").find('select').attr('disabled',false);
                        }
                        $("#costo_final_").attr('readonly',true);
                        $("#paciente_").attr('readonly',true);
                        $.each(event.becas,function(key,value){
                          $.each(value,function(ke,val){
                           porcentaje=val.cantidad;
                           if(porcentaje<1){
                            porcentaje=porcentaje*100;
                          }
                        });
                          if(value.monto_porcentaje!='monto'){ 
                           texto_beca_descuento='Porcentaje de Beca';
                           text_beca='porcentaje';
                           position='porcentaje';

                         }else{
                //costo_aux=costo-porcentaje;
                texto_beca_descuento='Monto del descuento';
                text_beca='monto';
                position='monto';
                
              }
              nextinput=nextinput+1;

              campo = '<div class="row" id="eliminarRow'+text_beca+''+nextinput+'">'+

              '<div class="col">'+

              '<label for="'+text_beca+nextinput+'">Becas</label>'+
              '</div>'+
              '<div class="col">'+

              ' <input id="'+text_beca+nextinput+'"name="orden'+value.orden+'[]" type="hidden" value="'+value.id+'">'+

              ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][beca_id]" type="hidden" value="'+value.id+'">'+            
              ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][monto_porcentaje]" type="hidden" value="'+value.monto_porcentaje+'">'+            
              ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][afecta]" type="hidden" value="'+value.afecta+'">'+

              ' <input id="'+text_beca+nextinput+'" class="form-control" readonly="readonly" name="x" type="text" value="'+value.descripcion+'">'+
              ' </div>'+

              ' <div class="col">'+

              ' <label for="'+text_beca+''+nextinput+'">'+texto_beca_descuento+'</label>'+
              ' </div>'+
              ' <div class="col">'+
              '<input class="form-control" id="'+text_beca+''+nextinput+'" name="becas['+nextinput+'][cantidad]" type="text" value="'+porcentaje+'" readonly>'+

              ' </div>'+
              ' <div class="col">'+
              '{!! Html::decode(Form::button('<i class="fas fa-minus-circle "></i>Eliminar Beca',['class' => 'btn btn-danger','id'=>'eliminarBeca_'])) !!}'+
              ' </div>'+
              '</div>';
              $("#orden_"+value.orden).empty();
              $("#orden_"+value.orden).append(campo);
              $("#eliminarBeca_").prop('id','eliminarBeca_'+text_beca+''+nextinput);
              $("#eliminarBeca_"+text_beca+""+nextinput).attr('onclick','eliminarBeca_("eliminarRow'+text_beca+''+nextinput+'")');

            });
                        $("#status_cita_").val(event.status_cita_id);
                            //$("#modalEvent #title").val();
                            $("#modalEvent").modal('show');
                          }

                        });

    function becasAjax(id,callback) {
      $.ajax({
        url:"{{route('events.becas')}}",
        type:"post",
        dataType:"json" ,
        data:{beca_id:id},
        success: function(response) {
          if(response.status==true){   
            m = response.beca;
            callback(m);
          }else{
            swal(response.costo);
          }
        }
      });

    }
    $('.fc-title').css('font-size', '1.85em');
    /////////////////////////AGREGA BECA EDICION
    $("#agregarBeca_").click(function(e){

      var id = $("#beca_descuento_").val();
      var porcentaje=$("#beca_porcentaje_").val();
      var costo = $("#costo_ini").val();
      var res="";
      var tiene_saldo="";
      becasAjax(id,function (callback){ 
        res=callback.monto_porcentaje;

        if(res!='monto'){

          porcentaje_aux=porcentaje/100;

          costo_aux=costo*porcentaje_aux;
          texto_beca_descuento='Porcentaje de Beca';
          text_beca='porcentaje';
          position='porcentaje';
          if(callback.apoyos_afecta!=null){
            if(callback.apoyos_afecta[0].saldo_final>=costo_aux){
              tiene_saldo="Si";
            }else{
              swal("No tiene saldo");
              revertFunc();

            }
          }

        }else{
          costo_aux=costo-porcentaje;
          texto_beca_descuento='Monto del descuento';
          text_beca='monto';
          position='monto';

        }

        nextinput=nextinput+1;
        campo = '<div class="row" id="eliminarRow'+text_beca+''+nextinput+'">'+

        '<div class="col">'+

        '<label for="'+text_beca+nextinput+'">Becas</label>'+
        '</div>'+
        '<div class="col">'+

        ' <input id="'+text_beca+nextinput+'"name="orden'+callback.orden+'[]" type="hidden" value="'+callback.id+'">'+

        ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][beca_id]" type="hidden" value="'+callback.id+'">'+            
        ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][monto_porcentaje]" type="hidden" value="'+callback.monto_porcentaje+'">'+            
        ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][afecta]" type="hidden" value="'+callback.afecta+'">'+

        ' <input id="'+text_beca+nextinput+'" class="form-control" readonly="readonly" name="x" type="text" value="'+callback.descripcion+'">'+
        ' </div>'+

        ' <div class="col">'+

        ' <label for="'+text_beca+''+nextinput+'">'+texto_beca_descuento+'</label>'+
        ' </div>'+
        ' <div class="col">'+
        '<input class="form-control" id="'+text_beca+''+nextinput+'" name="becas['+nextinput+'][cantidad]" type="text" value="'+porcentaje+'" readonly>'+

        ' </div>'+
        ' <div class="col">'+
        '{!! Html::decode(Form::button('<i class="fas fa-minus-circle "></i>Eliminar Beca',['class' => 'btn btn-danger','id'=>'eliminarBeca_'])) !!}'+
        ' </div>'+
        '</div>';
        $("#orden_"+callback.orden).append(campo);
        $("#eliminarBeca_").prop('id','eliminarBeca_'+text_beca+''+nextinput);
        $("#eliminarBeca_"+text_beca+""+nextinput).attr('onclick','eliminarBeca_("eliminarRow'+text_beca+''+nextinput+'")');
        $("#beca_porcentaje").prop('value',null);
            //Recalcular el valor
           // $("#costo__").val(costo);
           var datosform=$("#Becas_agregadas_ :input").serialize();
           $.ajax({
            url:"{{route('events.calculaBecas')}}",
            data:datosform,
            type:"post",
            dataType:"json",
            success:function(response){
              $("#costo_final_").val(response.precio);
            },
            error:function(){}
          });



         });
    });

    $("#agregarBeca").click(function(e){
      var id = $("#beca_descuento").val();
      var porcentaje=$("#beca_porcentaje").val();
      var costo = $("#costo").val();
      var res="";
      var tiene_saldo="";
      becasAjax(id,function (callback){ 
        res=callback.monto_porcentaje;
        if(res!='monto'){

          porcentaje_aux=porcentaje/100;

          costo_aux=costo*porcentaje_aux;
          texto_beca_descuento='Porcentaje de Beca';
          text_beca='porcentaje';
          position='porcentaje';
          if(callback.apoyos_afecta!=null){
            if(callback.apoyos_afecta[0].saldo_final>=costo_aux){
              tiene_saldo="Si";
            }else{
              swal("No tiene saldo");
              revertFunc();

            }
          }
          

        }else{
          costo_aux=costo-porcentaje;
          texto_beca_descuento='Monto del descuento';
          text_beca='monto';
          position='monto';

        }

        nextinput=nextinput+1;
        campo = '<div class="row" id="eliminarRow'+text_beca+''+nextinput+'">'+

        '<div class="col">'+

        '<label for="'+text_beca+nextinput+'">Becas</label>'+
        '</div>'+
        '<div class="col">'+

        ' <input id="'+text_beca+nextinput+'"name="orden'+callback.orden+'[]" type="hidden" value="'+callback.id+'">'+

        ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][beca_id]" type="hidden" value="'+callback.id+'">'+            
        ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][monto_porcentaje]" type="hidden" value="'+callback.monto_porcentaje+'">'+            
        ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][afecta]" type="hidden" value="'+callback.afecta+'">'+

        ' <input id="'+text_beca+nextinput+'" class="form-control" readonly="readonly" name="x" type="text" value="'+callback.descripcion+'">'+
        ' </div>'+

        ' <div class="col">'+

        ' <label for="'+text_beca+''+nextinput+'">'+texto_beca_descuento+'</label>'+
        ' </div>'+
        ' <div class="col">'+
        '<input class="form-control" id="'+text_beca+''+nextinput+'" name="becas['+nextinput+'][cantidad]" type="text" value="'+porcentaje+'" readonly>'+

        ' </div>'+
        ' <div class="col">'+
        '{!! Html::decode(Form::button('<i class="fas fa-minus-circle "></i>Eliminar Beca',['class' => 'btn btn-danger','id'=>'eliminarBeca'])) !!}'+
        ' </div>'+
        '</div>';
        $("#orden"+callback.orden).append(campo);
        $("#eliminarBeca").prop('id','eliminarBeca'+text_beca+''+nextinput);
        $("#eliminarBeca"+text_beca+""+nextinput).attr('onclick','eliminarBeca("eliminarRow'+text_beca+''+nextinput+'")');
        $("#beca_porcentaje").prop('value',null);
            //Recalcular el valor
            $("#costo__").val(costo);
            var datosform=$("#Becas_agregadas :input").serialize();
            $.ajax({
              url:"{{route('events.calculaBecas')}}",
              data:datosform,
              type:"post",
              dataType:"json",
              success:function(response){
                $("#costo_final").val(response.precio);
              },
              error:function(){}
            });

            

          });
    });
    $("#beca_descuento").change(function(){
     var id = $("#beca_descuento").val();
     $("#beca_porcentaje").prop('value',null);
     becasAjax(id,function (callback){ 
      if(callback.monto_porcentaje!='monto'){
                     swal("Agrega el porcentaje de 0 a 100");
                    $("#beca_porcentaje").prop("placeholder","Agrega el porcentaje de 0 a 100");
                  }else{
                     swal("Agrega el monto en peso");
                    $("#beca_porcentaje").prop("placeholder","Agrega el monto en pesos");
                  }
                });  

   });
        $("#beca_descuento_").change(function(){
     var id = $("#beca_descuento_").val();
     $("#beca_porcentaje").prop('value',null);
     becasAjax(id,function (callback){ 
      if(callback.monto_porcentaje!='monto'){
                     swal("Agrega el porcentaje de 0 a 100");
                    $("#beca_porcentaje").prop("placeholder","Agrega el porcentaje de 0 a 100");
                  }else{
                     swal("Agrega el monto en peso");
                    $("#beca_porcentaje").prop("placeholder","Agrega el monto en pesos");
                  }
                });  

   });
    $("#tipo_cita").change(function(){
      var consul = $("#consultor").val();
      var tipo_cita = $(this).children(":selected").text();
      var paciente_=$("#paciente").val();
      if(consul==null){
        swal("Elije un consultor");
        if(paciente_==null){
          swal("Elije un paciente");
        }
      }else{

    
        $.ajax({
          url:"{{route('consultor.price')}}",
          data:{consultor_id:consul,
            tipo_cita:tipo_cita
          },
          dataType:"json",
          type:"Post",
          success:function(response){
            if(response.status==true)   
            {
              $("#costo").val(response.costo);
              $("#costo_final").val(response.costo);

            }else{
              swal(response.costo);
              $("#costo").val(null);
            }
          },fail:function(){

          }

        });
      }

    });    $("#tipo_cita_").change(function(){
      var consul = $("#consultor_").val();
      var tipo_cita = $(this).children(":selected").text();
      var paciente_=$("#paciente_").val();

      if(consul==null){
        swal("Elije un consultor");
        if(paciente_==null){
          swal("Elije un paciente");
        }
      }else{

        if(tipo_cita=='diagnostico '){
          $("#ind_par").attr("disabled", true); 
        }
        $.ajax({
          url:"{{route('consultor.price')}}",
          data:{consultor_id:consul,
            tipo_cita:tipo_cita
          },
          dataType:"json",
          type:"Post",
          success:function(response){
            if(response.status==true)   
            {
              $("#costo").val(response.costo);
              $("#costo_final_").val(response.costo);
              $("#costo_ini").val(response.costo);

            }else{
             
              $("#costo").val(null);
            }
          },fail:function(){

          }

        });
      }

    });
    $("#ind_par").change(function(){
     var consul = $("#consultor").val();
     var individual_pareja = $(this).val();
     var tipo_cita=$("#tipo_cita").children(":selected").text();
     $.ajax({
      url:"{{route('consultor.price')}}",
      data:{consultor_id:consul,
        "individual_pareja":individual_pareja,
        "tipo_cita":tipo_cita
      },
      dataType:"json",
      method:"Post",
      success:function(response){
        if(response.status==true)    {   
          $("#costo").val(response.costo);
          $("#costo_final").val(response.costo);
        }else{
          swal(response.costo);
        }
      },fail:function(){

      }

    });
   });
        $("#individual_pareja_").change(function(){
     var consul = $("#consultor_").val();
     var individual_pareja = $(this).val();
     var tipo_cita=$("#tipo_cita_").children(":selected").text();
     $.ajax({
      url:"{{route('consultor.price')}}",
      data:{consultor_id:consul,
        "individual_pareja":individual_pareja,
        "tipo_cita":tipo_cita
      },
      dataType:"json",
      method:"Post",
      success:function(response){
        if(response.status==true)    {   
          $("#costo").val(response.costo);
           $("#costo_ini").val(response.costo);
          $("#costo_final_").val(response.costo);
        }else{
          swal(response.costo);
        }
      },fail:function(){

      }

    });
   });


    $("#consultor").change(function(){
     var consul = $("#consultor").val();
     var tipo_cita = $(this).children(":selected").text();
     var fecha=$("#date_start").val();
     var start=$("#time_start").val();
     var end=$("#date_end").val();
     $.ajax({
      url:"{{route('consultor.price')}}",
      data:{
        consultor_id:consul,
        tipo_cita:"seguimiento",
        fecha:fecha,
        start:start,
        end:end 
      },
      dataType:"json",
      method:"Post",
      success:function(response){

        $("#paciente").empty();
          $("#paciente").append('<option value="">Elije un paciente</option>');
            $(response.pacientes).each(function(index,pac){
              if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }
          $("#paciente").append('<option style="background:'+response.consultor.color+' " value="'+ pac.id +'">'+ pac.nombrecompleto+" - Pac "+pac.nombreconsultor+'</option>');
        });
        $(response.pacientes_).each(function(index,pac){
           if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }
          $("#paciente").append('<option value="'+ pac.id +'">'+ pac.nombrecompleto+" - Pac "+pac.nombreconsultor+'</option>');
   
        });
          $(response.pacientes_dif).each(function(index,pac){
            if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }
          $("#paciente").append('<option value="'+ pac.id +'">'+ pac.nombrecompleto+"  - Pac "+pac.nombreconsultor+'</option>');
        });
        if(response.status==true){  
          $("#costo").val(response.costo);
          $("#costo_final").val(response.costo);
          if(response.disponible!=true){
            swal("Este consultor no esta disponible en este horario")
          }
        }else{
          $("#costo").val(null);
        }
      },fail:function(){

      }

    });
   });

    $("#consultorios").change(function(){
      var consultorio_id=$(this).val();
      var fecha=$("#date_start").val();
      var start=$("#time_start").val();
      var end=$("#date_end").val();
      $.ajax({
        url:"{{route('consultorio.libre')}}",
        data:{
          consultorio_id:consultorio_id,
          fecha:fecha,
          start:start,
          end:end
        },
        type:"post",
        datatype:"json",
        success:function(response){
          if(response.status==true){
            swal("Este consultorio esta ocupado a esta hora");
          }
        },
        error:function(){}
      });
    });





  });


</script>
@endif   
@endsection
   
   
   
   
  
   
  