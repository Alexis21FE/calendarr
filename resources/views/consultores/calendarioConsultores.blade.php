    @extends('layouts.app')
    @section('content')

    
    <body>
        <div id="container">

            <div id="calendario_size"> 
               

<div id='calendar'></div>
{{Form::open(['route'=>'cita.update','method'=>'post','role'=>'form'])}}
{{Form::hidden('cita_id',null,['type'=>'hidden','id'=>'cita_id'])}}
<div id="modalEvent" class="modal" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4>Detalle de la cita</h4>

            </div>
            <div class="modal-body">
              <div class="row">

                <div class="col">

                    {{ Form::label('consultor','consultor') }}
                    {{ Form::select('Consultor', $consultores->pluck('nombre','id') , auth()->user()->consultor->id, ['id' => 'consultor',    'class'=>'form-control multiple ','readonly'=>'true']) }}
                </div>
                <div class="col">
                    {{ Form::label('paciente_','Paciente') }}
                    {{Form::text('paciente_',old('paciente_'),['class'=>'form-control', 'readonly'=>'true'])}}
                </div>

                <div class="col">
                    {{ Form::label('tipo_cita_', 'Tipo de Cita') }}
                    {{ Form::select('tipo_cita_', $tipo_citas, null, ['id' => 'tipo_cita_',    'class'=>'form-control multiple ','readonly'=>'true']) }}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {{ Form::label('date_start_','Fecha Cita') }}
                    {{Form::text('date_start_',old('date_start_'),['class'=>'form-control','type'=>'date','readonly'=>'true'])}}
                </div>

                <div class="col">
                    {{Form::label('time_start_','Hora inicio')}}
                    {{Form::time('time_start_',null,['class'=>'form-control','readonly'=>'true'])}}

                </div>
                <div class="col">
                    {{Form::label('date_end_','Hora fin')}}
                    {{Form::time('date_end_',null,['class'=>'form-control','readonly'=>'true'])}}
                </div>

            </div>
            <div class="row">
                <div class="col">
                 {{ Form::label('consultorio_','Consultorios') }}
                 {{ Form::select('consultorio_', $consultorios, null, ['id' => 'consultorio_',    'class'=>'form-control multiple ','readonly'=>'true']) }}
             </div>
             <div class="col">
                {{ Form::label('ind_par','Individual/Pareja') }}
                {{Form::select('individual_pareja_', ['Individual' => 'Individual', 'Pareja' => 'Pareja','Familiar'=>'Familiar'],null,['class'=>'form-control','placeholder' => 'Elija una opcion','id'=>'individual_pareja_','readonly'=>'true'])}}
            </div>
            <div class="col">
                {{ Form::label('status_cita_','Status Cita') }}
                {{ Form::select('status_cita_', $status_cita, null, ['id' => 'status_cita_',    'class'=>'form-control multiple ','readonly'=>'true']) }}
            </div>

        </div>
        
    <hr>
    <div id="Becas_agregadas_">
        {{Form::hidden('costo__',null,['type'=>'hidden','id'=>'costo_ini'])}}
        @for($x=1;$x<=$becas_descuentos->max('orden');$x++)
        <div id="orden_{{$x}}"></div>
        @endfor


    </div>
    <div class="row">
     <div class="col">
         {{ Form::label('cossto_inicial','Costo Inicial') }}
         {{ Form::text('costo_inicial',null,['id' => 'costo_inicial_','class'=>'form-control','readonly'=>'readonly']) }}
     </div>
        <div class="col">
         {{ Form::label('costo_final','Costo Total') }}
         {{ Form::text('costo_final',null,['id' => 'costo_final_','class'=>'form-control','readonly'=>'readonly']) }}
     </div>

 </div>         


</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" id="close_modal" data-dismiss="modal">Cancelar</button>
    {{Form::submit('Guardar',['class'=>'btn btn-success'])}}
      
    <a id="ir_cita" name="post" type="post" class="btn btn-primary">Ir a Cita</a>
  
</div>
</div>
</div>
</div>
{{Form::close()}}
</div>
</div>
</body>




<script>

  $(document).ready(function(){

    var nextinput=0;
    porcentaje='';
    $("#close_modal").modal("hide");
    var baseurl='{{url('/events/'.$consultor_id)}}';

    $('#calendar').fullCalendar({
        locale:'es',
        themeSystem:'bootstrap4',
        nowIndicator:true,
        slotLabelFormat:'h:mm a',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate: '{{date('Y-m-d')}}',
                          navLinks: true, // can click day/week names to navigate views
                          editable: true,
                          defaultView: 'agendaWeek',
                          minTime:'09:00:00',
                          maxTime:'23:00:00',
                          allDayText:'Horario',
                          selectable:true,
                          selectHelper:true,
                          eventRender: function(event, element)
                          { 
                            if(event.status_cita.descripcion=='confirmada'){
                             element.find('.fc-time').prepend("<span><i class='fas fa-check-circle'></i></span>");
                           } 
                            if(event.status_cita.descripcion=='cancelada'){
                            element.find('.fc-content').css('text-decoration','line-through');
                           }
                       
                         if(event.paciente.alias!=''){

                            element.find('.fc-content').append("<div class='fc-time'><span>"+event.consultor.nombre+" "+event.paciente.nombrecompleto+"</span></div>");
                           } else{

                            element.find('.fc-content').append("<div class='fc-time'><span>"+event.consultor.nombre+" "+event.paciente.nombrecompleto+"</span></div>");

                           }
                            element.find('.fc-content').append("<span>"+event.status_cita.descripcion+" "+event.monto_final+"</span>"); 
                        },
                        eventDrop: function(event, delta, revertFunc) {
 if (!confirm("Estas seguro de modificar la cita?")) {
      revertFunc();
    }else{
        fecha_nueva=event.start.format('YYYY-MM-DD');
        hora_inicio_nueva=event.start.format('HH:mm:ss');
        hora_fin_nueva=event.end.format('HH:mm:ss');
        cita_id=event.id;
        $.ajax({
            url:"{{route('update.Fecha')}}",
            data:{
                fecha_nueva:fecha_nueva,
hora_inicio_nueva:hora_inicio_nueva,
hora_fin_nueva:hora_fin_nueva,
cita_id:cita_id
            },dataType:'json',
            type:'post',
            success: function(response){
                if(response.status==true){
                alert(response.mensaje);
                }else{
                    alert(response.mensaje);
                }
            },
            error:function(){

            }

        });

    }
  },
                        select:function(start){
                            start=moment(start.format());
                            $("#date_start").val(start.format('YYYY-MM-DD'));
                            $("#time_start").val(start.format('HH:mm'));
                            $("#date_end").val(start.format('HH:mm:ss'));
                            var dateend=$("#date_end").val();
                            
                            dateend=parseFloat(dateend)+parseFloat(1);
                            $("#date_end").val(dateend+':00');
                            $("#responsive-modal").modal('show');
                        },
                        events: baseurl, 
                        eventClick:function(event,jsEvent,view){
                            var date_start = $.fullCalendar.moment(event.start).format('YYYY-MM-DD');
                            var time_start = $.fullCalendar.moment(event.start).format('HH:mm');
                            var date_end = $.fullCalendar.moment(event.end).format('HH:mm');
                            $("#modalEvent #title").val(event.descripcion);
                            $("#modalEvent #date_start_").val(date_start);
                            $("#modalEvent #time_start_").val(time_start);
                            $("#modalEvent #date_end_").val(date_end);
                            $("#consultor_").val(event.consultor_id);
                            if(event.paciente.apellido_pa!=''){
                                var ap_pa=event.paciente.apellido_pa;
                            }else{
                                ap_pa='';
                            } if(event.paciente.apellido_ma!=''){
                                var ap_ma=event.paciente.apellido_ma;
                            }else{
                                ap_ma='';
                            }
                            $("#paciente_").val(event.paciente.nombre+' '+ap_pa+' '+ap_ma);
                            $("#tipo_cita_").val(event.tipo_cita_id);
                            $("#consultorio_").val(event.consultorio_id);
                            $("#individual_pareja_").val(event.individual_pareja);
                            $("#costo_final_").val(event.monto_final);
                            $("#costo_inicial_").val(event.monto_inicial);
                            $("#costo_ini").val(event.monto_inicial);
                            $("#cita_id").val(event.id);
                            $("#ir_cita").attr('href',"{{route('consultores.pacientes','')}}"+"/"+event.id);
                              $("#Becas_agregadas_").children('div').empty();
                            $.each(event.becas,function(key,value){
                                $.each(value,function(ke,val){
                                   porcentaje=val.cantidad;
                                   if(porcentaje<1){
                                    porcentaje=porcentaje*100;
                                }
                            });
                                if(value.monto_porcentaje!='monto'){ 
                                 texto_beca_descuento='Porcentaje de Beca';
                                 text_beca='porcentaje';
                                 position='porcentaje';

                             }else{
                //costo_aux=costo-porcentaje;
                texto_beca_descuento='Monto del descuento';
                text_beca='monto';
                position='monto';
                
            }
            nextinput=nextinput+1;

            campo = '<div class="row" id="eliminarRow'+text_beca+''+nextinput+'">'+

            '<div class="col">'+

            '<label for="'+text_beca+nextinput+'">Becas</label>'+
            '</div>'+
            '<div class="col">'+

            ' <input id="'+text_beca+nextinput+'"name="orden'+value.orden+'[]" type="hidden" value="'+value.id+'">'+

            ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][beca_id]" type="hidden" value="'+value.id+'">'+            
            ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][monto_porcentaje]" type="hidden" value="'+value.monto_porcentaje+'">'+            
            ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][afecta]" type="hidden" value="'+value.afecta+'">'+

            ' <input id="'+text_beca+nextinput+'" class="form-control" readonly="readonly" name="x" type="text" value="'+value.descripcion+'">'+
            ' </div>'+

            ' <div class="col">'+

            ' <label for="'+text_beca+''+nextinput+'">'+texto_beca_descuento+'</label>'+
            ' </div>'+
            ' <div class="col">'+
            '<input class="form-control" id="'+text_beca+''+nextinput+'" name="becas['+nextinput+'][cantidad]" type="text" value="'+porcentaje+'" readonly>'+

            ' </div>'+
            ' <div class="col">'+
            ' </div>'+
            '</div>';
            $("#orden_"+value.orden).empty();
            $("#orden_"+value.orden).append(campo);
            $("#eliminarBeca_").prop('id','eliminarBeca_'+text_beca+''+nextinput);
            $("#eliminarBeca_"+text_beca+""+nextinput).attr('onclick','eliminarBeca_("eliminarRow'+text_beca+''+nextinput+'")');

        });
                            $("#status_cita_").val(event.status_cita_id);
                            //$("#modalEvent #title").val();
                            $("#modalEvent").modal('show');
                        }
                        
                    });

    $('.fc-content .fc-time').css('font-size','1em');
    function becasAjax(id,callback) {
        $.ajax({
          url:"{{route('events.becas')}}",
          type:"post",
          dataType:"json" ,
          data:{beca_id:id},
          success: function(response) {
            if(response.status==true){   
                m = response.beca;
                callback(m);
            }else{
                alert(response.costo);
            }
        }
    });

    }




});


</script>
@endsection
      