@extends('../layouts.app')
@section('content')
<div class="container">

	 	{{Form::open(['route'=>'especialidades.post','method'=>'post','role'=>'form','id'=>'form__'])}}
	<div class="row">
		<div class="col">
	{{Form::label('consultor','Especialidad')}}
		</div>
		<div class="col">
			@php
			if(isset($isResponse)){

			$res=$especialidades_response->first()->id;
			}else{
				$res=null;
			}
			@endphp
		{{Form::select('especialidad_id',$especialidades,$res,['class'=>'form-control','id'=>'especialidades_','placeholder'=>'Seleccione una especialidad'])}}
			
		</div>
		<div class="col"></div>
		<div class="col"></div>
	</div>
	<hr>
	<div id="response">
	{{Form::close()}}
	 @if(isset($isResponse))
<br>
	<table class="table" id="table2excel">
      <thead class="thead-dark">
       <tr>

        <th scope="col">Consultor</th>
        <th scope="col">Especialidad</th>
        <th scope="col">Telefono</th>
    </tr>
    	    </thead>
    	    <tbody>
    	    	@foreach($especialidades_response->first()->consultores as $consultor)
    	    	<tr>
    	    		<td>{{$consultor->nombre." ".$consultor->apellido_pa." ".$consultor->apellido_ma}}</td>
    	    		<td>{{$especialidades_response->first()->descripcion}}</td>
    	    		<td>{{$consultor->celular_paciente}}</td>
    	    	</tr>
    	    	@endforeach
    	    </tbody>
</table>
	 @endif
			
	</div>
</div>
<script type="text/javascript">

$(document).ready(function(){
	$("#especialidades_").change(function(){
		$("#response").empty();
		         $('<div class="progress" id="gif" style="width:30%;display: none;">'+
  '<div id="bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 40%">Espera un momento...</div>').insertBefore("#response");
            $("#gif").css("display","flex");
            for (var i=0; i<750; i+=2) {
            x=i/10;
            i=i-1;

             $("#bar").css("width",x+"%");
}
		$("#form__").submit();
	});
});
</script>
@endsection