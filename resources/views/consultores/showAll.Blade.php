@extends('../layouts.app')
@section('content')
<div class="container">
	{{Form::label('consultor','Consultor')}}
	<div class="row">
		<div class="col">
			
	<select class="form-control" id="select_consultor">

		  <option value="" disabled selected hidden>Elija un consultor</option>
	@foreach($consultores as $consultor)
	<option value="{{$consultor->id}}">{{$consultor->nombre." ".$consultor->apellido_pa." ".$consultor->apellido_ma}}</option>
	
	@endforeach
	</select>
		</div>
		<div class="col"></div>
		<div class="col"></div>
	</div>
	<div id="response">
		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#select_consultor").change(function(){
		var consultor_id=$(this).val();
		$.ajax({
			url:"{{route('consultor.showConsultor')}}",
			data:{consultor_id:consultor_id},
			datatype:'json',
			type:'post',
			success:function(response){
				$("#gif").remove();
				$("#response").html(response);
			},
			error:function(){}
		});
	});

	 			
});

</script>
@endsection