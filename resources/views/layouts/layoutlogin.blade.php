<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@php
setlocale(LC_ALL,"es_ES");
@endphp
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--meta name="csrf-token" content="{{ csrf_token() }}">
        -   -->
        <title>{{ config('app.name', 'Family') }}</title>
        <link rel="shortcut icon" href="{{url('/img/FullSizeRender.jpg')}}" />

        <!-- Styles -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

        {!!Html::script('vendor/seguce92/jquery.min.js')!!}  
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    

        {!!Html::script('js/sweetalert2.all.min.js')!!}
        <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
        {!!Html::style('css/own.css')!!}
        {!!Html::style('css/app.css')!!}
        
        {!!Html::style('vendor/seguce92/fullcalendar/fullcalendar.min.css')!!}
        
        {!!Html::style('vendor/seguce92/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css')!!}
        {!!Html::style('vendor/seguce92/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')!!} 
        {!!Html::script('vendor/seguce92/fullcalendar/lib/moment.min.js')!!}  
        {!!Html::script('vendor/seguce92/fullcalendar/fullcalendar.min.js')!!}
        {!!Html::script('vendor/seguce92/fullcalendar/locale/es.js')!!}
        {!!Html::script('vendor/seguce92/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js')!!}
        {!!Html::script('vendor/seguce92/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')!!}
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
        {!!Html::script('js/excel_table/src/table2excel.js')!!}
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
        <!-- Theme included stylesheets -->


        <!--script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> 

    
           CSRF Token -->


       </head>  
       <body class="bak">
        <div id="app">
            @guest
            @else
            @include('../partials/navmenu')
            @endguest
             <img src="{{url('/img/logo.png')}}" style="margin-top: 3% ;margin-bottom: 2%; margin-left: 38%">
            <div id="calen-dir">
                @yield('content')
                
            </div>
            
        </div>
        <div id="footer">
            <br>
        </div>
        <!-- Scripts -->

        {!!Html::script('vendor/tinymce/js/tinymce/tinymce.min.js')!!}
        <!-- Main Quill library -->


    </body>
</script>
</html>
