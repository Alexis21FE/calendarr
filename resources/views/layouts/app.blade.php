<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@php
setlocale(LC_ALL,"es_ES");
@endphp
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--meta name="csrf-token" content="{{ csrf_token() }}">
        -   -->
        <title>{{ config('app.name', 'Family') }}</title>
        <link rel="shortcut icon" href="{{url('/img/FullSizeRender.jpg')}}" />

        <!-- Styles -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

        {!!Html::script('vendor/seguce92/jquery.min.js')!!}  
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    

        {!!Html::script('js/sweetalert2.all.min.js')!!}
        <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
        {!!Html::style('css/own.css')!!}
        {!!Html::style('css/app.css')!!}
        
        {!!Html::style('vendor/seguce92/fullcalendar/fullcalendar.min.css')!!}
        
        {!!Html::style('vendor/seguce92/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css')!!}
        {!!Html::style('vendor/seguce92/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')!!} 
        {!!Html::script('vendor/seguce92/fullcalendar/lib/moment.min.js')!!}  
        {!!Html::script('vendor/seguce92/fullcalendar/fullcalendar.min.js')!!}
        {!!Html::script('vendor/seguce92/fullcalendar/locale/es.js')!!}
        {!!Html::script('vendor/seguce92/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js')!!}
        {!!Html::script('vendor/seguce92/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')!!}
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
        {!!Html::script('js/excel_table/src/table2excel.js')!!}
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
        <!-- Theme included stylesheets -->


        <!--script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> 

    
           CSRF Token -->


       </head>
       <body>
        <div id="app">
            @guest
            @else
            @include('../partials/navmenu')
            @endguest
            <div id="calen-dir">
                @yield('content')
                
            </div>
            
        </div>
        <div id="footer">
            <br>
        </div>
        <!-- Scripts -->

        {!!Html::script('vendor/tinymce/js/tinymce/tinymce.min.js')!!}
        <!-- Main Quill library -->


    </body>
    <script type="text/javascript" charset="utf-8" >
        $("input").click(function () {
    $("#" + this.id).focus();
});
      tinymce.init({
          selector: 'textarea',
          resize:true,
          menubar: false,
             hidden_input: false,
     setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    },
          plugins: [
          'advlist autolink lists link image charmap print preview anchor textcolor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table contextmenu paste code help wordcount'
          ],
          toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
          content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css']
      });
      $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
            var x;
            $('<div class="progress" id="gif" style="width:30%;display: none;">'+
  '<div id="bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 40%">Espera un momento...</div>').insertBefore("#response");
            $("#gif").css("display","flex");
            for (var i=0; i<750; i+=2) {
            x=i/10;
            i=i-1;
             $("#bar").css("width",x+"%");
}
           
        },
    });
      function eliminarBeca(id){
        $("#"+id).remove();
        var datosform=$("#Becas_agregadas :input").serialize();
        $.ajax({
            url:"{{route('events.calculaBecas')}}",
            data:datosform,
            type:"post",
            dataType:"json",
            success:function(response){
                $("#costo_final").val(response.precio);
            },
            error:function(){}
        });

    }
    function eliminarBeca_(id){
        $("#"+id).remove();
        var datosform=$("#Becas_agregadas_ :input").serialize();
        console.log(datosform);
        $.ajax({
            url:"{{route('events.calculaBecas')}}",
            data:datosform,
            type:"post",
            dataType:"json",
            success:function(response){
                $("#costo_final_").val(response.precio);
            },
            error:function(){}
        });

    }
    $(".interruptor-tecla").click(function(){
        if($("#prendido").prop('checked')){
            $.ajax({
                url:"{{route('home.con')}}",
                type:"post",
                dataType:"json",
                data:{calendario_type:"director"},
                success:function(response){
                    if(response.status==true){
                        window.location.reload();
                    }
                },
                error:function(){}
            });
        }
        if($("#apagado").prop('checked')){
            $.ajax({
                url:"{{route('home.con')}}",
                type:"post",
                dataType:"json",
                data:{calendario_type:"consultor"},
                success:function(response){
                    if(response.status==true){
                        window.location.reload();
                    }
                },
                error:function(){}
            });
        }                
    });
    
</script>
</html>
