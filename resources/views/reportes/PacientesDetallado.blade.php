    @extends('layouts.app')
    @section('content')
@php
              if(isset($isResponse)){
        $consultor_id=$consultor;
        $fecha_ini=$inicio;
     
    }else{
        $consultor_id=null;
         $fecha_ini=null;
    }
  @endphp
 <div class="section">
      <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Detallado por paciente</p>
        </center>
        {{Form::open(['route'=>'reporte.consultaPacientes','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
            <div class="row" style="margin: auto;">

 <div class="col">{{Form::label('consultor','Consultor')}}
                {{Form::select('consultor',$consultores->pluck('nombrecompleto','id'),$consultor_id,['class'=>'form-control','id'=>'consultor_select','placeholder'=>'Selecciona un consultor'])}}</div>
            
                <div class="col">
                  {{Form::label('paciente','Paciente')}}
                     <select id ='paciente' onChange="calcularReporte_()"  class="form-control" name="paciente" required="true">
                        <option value="" disabled selected hidden>Elija un paciente</option>
                        @if(empty($paciente_response))
                       @foreach($pacientes as $paciente)
                       <option value="{{$paciente->id}}">{{empty($paciente->alias)?$paciente->nombre." ".$paciente->apellido_pa." ".$paciente->apellido_ma." - Pac ".$paciente->nombreconsultor:$paciente->alias." - Pac ".$paciente->nombreconsultor}}</option>
                       @endforeach
                       @else
                        @foreach($pacientes as $paciente)
                       <option value="{{$paciente->id}}" {{$paciente->id==$paciente_response->id?'selected="selected"':''}}>
                           {{empty($paciente->alias)?$paciente->nombre." ".$paciente->apellido_pa." ".$paciente->apellido_ma." - Pac ".$paciente->nombreconsultor:$paciente->alias." - Pac ".$paciente->nombreconsultor}}
                       </option>
                       @endforeach
                       @endif
                     </select>
                </div>

            <div class="col">
                {{Form::label('fecha_ini','Fecha Inicial')}}
                {{Form::date('fecha_ini',$fecha_ini,['class'=>'form-control','type'=>'date','required'=>'required','id'=>'fecha_ini','onChange'=>'calcularReporte_()'])}}
            </div>
            <div class="col">
                {{Form::label('fecha_fin','Fecha final')}}
                {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required'])}}

            </div>
        </div>
    {{Form::close()}}
    <hr>
    <div id="response">
@if(isset($isResponse))
  @if($paciente_response==null)
  <center><h1>No hay datos</h1 ></center>
  @else
   <table class="table" id="table2excel">
      <thead class="thead-dark">
         <tr>
            <th scope="col">Consultor</th>
            <th scope="col">Paciente</th>
            <th scope="col">Fecha</th>
            <th scope="col">No. Sesion</th>
            <th scope="col">Telefono</th>
        </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{!empty($paciente_response->diagnosticos)?$paciente_response->diagnosticos->consultor->nombre.' '.$paciente_response->diagnosticos->consultor->apellido_pa.' '.$paciente_response->diagnosticos->consultor->apellido_ma:''}}</td>
        <td>{{empty($paciente_response->alias)?$paciente_response->nombre." ".$paciente_response->apellido_pa." ".$paciente_response->apellido_ma :$paciente_response->alias}}</td>
        <td>{{$paciente_response->created_at->format('d/m/Y')}}</td>
        <td>{{!empty($paciente_response->diagnosticos)?'0':'0'}}</td>
        <td>{{$paciente_response->celular_paciente}}</td>
        
    </tr>
  @foreach($paciente_response->citas as $seguimiento)
     <tr>
       <td>{{!empty($paciente_response->seguimientos->first())?$paciente_response->seguimientos->first()->consultor->nombre.' '.$paciente_response->diagnosticos->consultor->apellido_pa.' '.$paciente_response->diagnosticos->consultor->apellido_ma:''}}</td>
        <td>{{empty($paciente_response->alias)?$paciente_response->nombre." ".$paciente_response->apellido_pa." ".$paciente_response->apellido_ma :$paciente_response->alias}}</td>
        <td>{{$seguimiento->created_at->format('d/m/Y')}}</td>
        <td>{{$seguimiento->no_sesion}}</td>
        <td>{{$paciente_response->celular_paciente}}</td>
    </tr>
    @endforeach
</tbody>
</table>
<button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
@endif
</div>
</div>
@endif
</div>
    <script type="text/javascript">
    	
        $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

  });

});
             $(document).ready(function(){
              $("#consultor_select").change(function(){
                   $("#response").empty();
                $.ajax({
                  url:"{{route('dame.pacientes')}}",
                  datatype:"json",
                  type:"post",
                  data:{consultor:$(this).val()},
                  success:function(response){

                    $("#gif").remove();
                          $("#paciente").empty();
                           $("#paciente").append('<option>Selecciona un paciente</option>');
       
            $(response.pacientes).each(function(index,pac){

              if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }

          $("#paciente").append('<option style="background:'+pac.color+' " value="'+ pac.id +'">'+ pac.nombrecompleto+"- Pac "+pac.nombreconsultor+'</option>');
        });
                  },
                  error:function(){}
                });
              });

          
        
        });
function calcularReporte_(){
  var fecha_ini=$("#fecha_ini").val();
  if(fecha_ini!='' && $("#paciente").val()!=null){

    $("#response").empty();
                 $('<div class="progress" id="gif" style="width:30%;display: none;">'+
  '<div id="bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 40%">Espera un momento...</div>').insertBefore("#response");
            $("#gif").css("display","flex");
            for (var i=0; i<750; i+=2) {
            x=i/10;
            i=i-1;

             $("#bar").css("width",x+"%");
    }
           $("#calculaReporte").submit();
      
  }
            
    
}
    </script>
    @endsection