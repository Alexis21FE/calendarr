    @extends('layouts.app')
    @section('content')
    <div class="section">
     <center>

        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Mis datos</p>
    </center>
    <div id="as" style="width: 40%; margin: auto;">

        <div class="row" style="padding-right: 25%;
        padding-left: 25%;">
        <div class="col">
        {{Form::hidden('mail',$datos->email,['id'=>'mail'])}}
         {{Form::label('nombre','Nombre')}}
         {{Form::text('nombre',$datos->name,['id'=>'nombre','class'=>'form-control'])}}
     </div>

 </div>
 <div id="divpass" style="display:none">
    <div class="row" style="padding-right: 25%;
    padding-left: 25%;">
    <div class="col">
        {{Form::label('password','Contraseña')}}
        {{Form::text('pass',null,['class'=>'form-control'])}}
    </div>
</div>

<div class="row" style="padding-right: 25%;
padding-left: 25%;">
<div class="col">
    {{Form::label('password','Contraseña')}}
    {{Form::text('pass_',null,['class'=>'form-control'])}}
</div>
</div>
</div>

<div class="row" style="padding-right: 25%;
padding-left: 25%;margin-top: 5%;">
<div class="col">
    {{Form::button('Contraseña',['id'=>'cambiar_pass','class'=>'btn btn-warning','style'=>'width:100%;'])}}
</div>
</div>
<div class="row" style="padding-right: 25%;
padding-left: 25%;margin-top: 5%;">
<div class="col">
    {{Form::button('Guardar',['id'=>'guardar','class'=>'btn btn-success','style'=>'width:100%;'])}}
</div>
</div>


<div id="response">

</div>
</div>
</div>
<script type="text/javascript">
    $("#cambiar_pass").click(function(){
        $("#divpass").css("display","block");
    });
    $("#guardar").click(function(){


    $mail=$("#mail").val();
    $pass=$("#pass").val();
    $pass_=$("#pass_").val();
    $nombre=$("#nombre").val();
    if($pass==null && $pass_==null){
         $.ajax({
                url:"{{route('change.datos')}}",
                type:"post",
                dataType:"json",
                data:{
                    mail:$mail,
                    nombre:$nombre
                },
                success:function(response){
                    $("#gif").remove();
                    swal(response.msg);
                },
                error:function(){}
            });
    }else{
  /*  if($pass.match(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/))
    {
        if($pass==$pass_){
            $.ajax({
                url:"{{route('change.datos')}}",
                type:"post",
                dataType:"json",
                data:{
                    pass:$pass,
                    mail:$mail
                },
                success:function(response){
                    swal(response.msg);
                },
                error:function(){}
            });
        }else{
            swal("Las contraseñas no coinciden");
        }
    }else{
        swal("Debe de contener 8 caracteres mayusculas y numeros ");
    }*/
}
        });
</script>
@endsection