    @extends('layouts.app')
    @section('content')
  @if($reporte=='Casos')

    <div class="section">
        {{Form::open(['route'=>'reporte.diario','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
        <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Casos</p>
        </center>
            <div class="row" style="margin:auto;">
        
        <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini',null,['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
        </div>
        <div class="col"></div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
            
        </div>
        <div class="col"></div>
    </div>
    {{Form::close()}}
    <hr>
 <div id="response">
     
 </div>
 <script type="text/javascript">
     function calculaReporte(){
         $("#response").empty();
        $fecha_ini=$("#fecha_ini").val();
        $fecha_fin=$("#fecha_fin").val();
        if($fecha_ini=='' || $fecha_fin==''){
            swal("debes de seleccionar ambas fechas");
        }else{
            $.ajax({
                url:"{{route('reporte.Casos_p')}}",
              data:{
                    fecha_ini:$fecha_ini,
                    fecha_fin:$fecha_fin
                },
                datatype:'json',
                type:'post',
                success:function(response){
                   
                    $("#response").html(response);
                    $("#gif").remove();
                },
                error:function(){}
            });
        }
     }
 </script>
 @elseif($reporte=='Diagnosticos')
  <div class="section">
        {{Form::open(['route'=>'reporte.diario','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
        <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Diagnosticos</p>
        </center>
            <div class="row" style="margin:auto;">
        
        <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini',null,['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
        </div>
        <div class="col"></div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
            
        </div>
        <div class="col"></div>
    </div>
    {{Form::close()}}
    <hr>
 @elseif($reporte=='Citas_turno')
   <div class="section">
        {{Form::open(['route'=>'reporte.diario','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
        <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Citas por turno</p>
        </center>
            <div class="row" style="margin:auto;">
        
        <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini',null,['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
        </div>
        <div class="col"></div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
            
        </div>
        <div class="col"></div>
    </div>
    {{Form::close()}}
    <hr>
     <div id="response">
     
 </div>
     <script type="text/javascript">
     function calculaReporte(){
        $fecha_ini=$("#fecha_ini").val();
        $fecha_fin=$("#fecha_fin").val();
        if($fecha_ini=='' || $fecha_fin==''){
            swal("debes de seleccionar ambas fechas");
        }else{

                    $("#response").empty();
            $.ajax({
                url:"{{route('reporte.Citas_turno_p')}}",
              data:{
                    fecha_ini:$fecha_ini,
                    fecha_fin:$fecha_fin
                },
                datatype:'json',
                type:'post',
                success:function(response){
                    $("#response").html(response);
                     $("#gif").remove();
                },
                error:function(){}
            });
        }
     }
 </script>
 @elseif($reporte=='Horas_Consultor')
   <div class="section">
        {{Form::open(['route'=>'reporte.diario','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
        <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Horas por consultor</p>
        </center>
            <div class="row" style="margin:auto;">
        
        <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini',null,['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
        </div>
        <div class="col"></div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
            
        </div>
        <div class="col"></div>
    </div>
    {{Form::close()}}
    <hr>

<div id="response"></div>
</div>
    <script type="text/javascript">
    	 function calculaReporte(){
        $fecha_ini=$("#fecha_ini").val();
        $fecha_fin=$("#fecha_fin").val();
        if($fecha_ini=='' || $fecha_fin==''){
            swal("debes de seleccionar ambas fechas");
        }else{
                   $("#response").empty();
            
            $.ajax({
                url:"{{route('reporte.Horas_Consultor_p')}}",
              data:{
                    fecha_ini:$fecha_ini,
                    fecha_fin:$fecha_fin
                },
                datatype:'json',
                type:'post',
                success:function(response){
 $("#gif").remove();
                    $("#response").html(response);
                },
                error:function(){}
            });
        }
     }
        $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

  });
    });

    </script>
    @elseif($reporte=='seguimiento_consultor')
     <div class="section">
         <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Seguimientos por consultor</p>
        </center>
         <div class="row">
<div class="col"></div>
            <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini',null,['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
        </div>
        <div class="col"></div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
            
        </div>
        <div class="col"></div>
     </div>
     <div id="response"></div>
     </div>
      <script type="text/javascript">
         function calculaReporte(){
        $fecha_ini=$("#fecha_ini").val();
        $fecha_fin=$("#fecha_fin").val();
        if($fecha_ini=='' || $fecha_fin==''){
            swal("debes de seleccionar ambas fechas");
        }else{
                   $("#response").empty();
            
            $.ajax({
                url:"{{route('reporte.seguimientos_p')}}",
              data:{
                    fecha_ini:$fecha_ini,
                    fecha_fin:$fecha_fin
                },
                datatype:'json',
                type:'post',
                success:function(response){
 $("#gif").remove();
                    $("#response").html(response);
                },
                error:function(){}
            });
        }
     }
        $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

  });
    });

    </script>
     @endif
    @endsection