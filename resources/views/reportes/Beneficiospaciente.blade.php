    @extends('layouts.app')
    @section('content')
    @php
     if(isset($isResponse)){
        $fecha_ini=$inicio;
    }else{
           $fecha_ini=0;
    }
    $x=0;
    @endphp

    <div class="section">
      <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Beneficios por paciente</p>
        </center>
        {{Form::open(['route'=>'reporte.beneficiosConsulta','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
            <div class="row" style="margin:auto;">
        
        <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini',$fecha_ini,['class'=>'form-control','type'=>'date','required'=>'required','id'=>'fecha_ini','onChange'=>'calculaReporte()'])}}
        </div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required'])}}

            
        </div>
        <div class="col">
            {{Form::label('pacietne_id','Paciente')}}
               <select id ='paciente'   class="form-control" name="Paciente" required='required' onChange="calculaReporte()">
                        <option value="" disabled selected hidden>Elija un paciente</option>

                        @if(empty($isResponse) || empty($becas_descuentos->first()))

                       @foreach($pacientes as $paciente)
                       <option value="{{$paciente->id}}">{{empty($paciente->alias)?$paciente->nombre." ".$paciente->apellido_pa." ".$paciente->apellido_ma." - Pac. ".$paciente->nombreconsultor:$paciente->alias." - Pac. ".$paciente->nombreconsultor}}</option>
                       @endforeach
                       @else
                      
                        @foreach($pacientes as $paciente)
                       
                        
                       <option value="{{$paciente->id}}" {{$paciente->id==$becas_descuentos->first()->citas_reg->paciente->id?'selected="selected"':''}}>
                           {{empty($paciente->alias)?$paciente->nombre." ".$paciente->apellido_pa." ".$paciente->apellido_ma." - Pac. ".$paciente->nombreconsultor:$paciente->alias." - Pac. ".$paciente->nombreconsultor}}
                       </option>
                      
                       @endforeach
                       @endif
                     </select> 
        </div>
    </div>
    {{Form::close()}}
    <hr>
<div id="response">
@if(isset($isResponse) && !empty($becas_descuentos))
   <table class="table" id="table2excel">
      <thead class="thead-dark">
         <tr>
            <th scope="col">fecha</th>
            <th scope="col">Paciente</th>
            <th scope="col">Colegio</th>
            <th scope="col">Beca/Apoyo</th>
            <th scope="col">Importe</th>
            <th scope="col">Consultor</th>
        </tr>
    </thead>
    <tbody>
  @foreach($becas_descuentos as $beca_descuento)
  @php
             $fecha=$beca_descuento->citas_reg->fecha_cita;
        $fecha_actual = new DateTime($fecha);
        $cadena_fecha_actual = $fecha_actual->format("d/m/Y");@endphp
        @if($beca_descuento->cita_id==$beca_descuento->citas_reg->paciente->diagnosticos->cita_id)
     <tr>
        <td>{{$cadena_fecha_actual}}</td>
        <td>{{empty($beca_descuento->citas_reg->paciente->alias)?$beca_descuento->citas_reg->paciente->nombre." ".$beca_descuento->citas_reg->paciente->apellido_pa:$beca_descuento->citas_reg->paciente->alias}}</td>
        <td>{{$beca_descuento->citas_reg->paciente->colegio}}</td>
        <td>{{$beca_descuento->becas_reg->descripcion}}</td>
        <td>${{$beca_descuento->equivalente}}</td>
          <td>{{$beca_descuento->citas_reg->paciente->diagnosticos->consultor->nombre." ".$beca_descuento->citas_reg->paciente->diagnosticos->consultor->apellido_pa." ".$beca_descuento->citas_reg->paciente->diagnosticos->consultor->apellido_ma}}</td>
        <td>{{$beca_descuento->citas_reg->paciente->diagnosticos->created_at->format('d/m/Y')}}</td>
        <td>0</td>
        <td>{{$beca_descuento->citas_reg->paciente->celular_paciente}}</td>
    </tr>
    @else
      <td>{{$cadena_fecha_actual}}</td>
        <td>{{empty($beca_descuento->citas_reg->paciente->alias)?$beca_descuento->citas_reg->paciente->nombre." ".$beca_descuento->citas_reg->paciente->apellido_pa:$beca_descuento->citas_reg->paciente->alias}}</td>
         <td>{{$beca_descuento->citas_reg->paciente->colegio}}</td>
        <td>{{$beca_descuento->becas_reg->descripcion}}</td>
        <td>${{$beca_descuento->equivalente}}</td>
          <td>{{$beca_descuento->citas_reg->consultor->nombre." ".$beca_descuento->citas_reg->consultor->apellido_pa." ".$beca_descuento->citas_reg->consultor->apellido_ma}}</td>
          @if(!empty($beca_descuento->citas_reg->paciente->seguimiento))
        <td>{{$beca_descuento->citas_reg->paciente->seguimiento->created_at->format('d/m/Y')}}</td>
        <td>{{$beca_descuento->citas_reg->paciente->seguimiento->no_sesion}}</td>
        <td>{{$beca_descuento->citas_reg->paciente->celular_paciente}}</td>
        @endif
    </tr>
    @endif
    @endforeach
</tbody>
</table>
<button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
</div>
</div>
@endif
</div>
    <script type="text/javascript">
        
        $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

  });

});
function  calculaReporte(){
    var fail_log='';
    var fail=false;
  $( '#calculaReporte' ).find( 'select, textarea, input' ).each(function(){
            if( ! $( this ).prop( 'required' )){

            } else {
                if ( ! $( this ).val() ) {
                    fail = true;
                    name = $( this ).attr( 'name' );
                    fail_log += name + " es requerido \n";
                }

            }
        });

        //submit if fail never got set to true
        if ( ! fail ) {
          $("#response").empty();
                 $('<div class="progress" id="gif" style="width:30%;display: none;">'+
  '<div id="bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 40%">Espera un momento...</div>').insertBefore("#response");
            $("#gif").css("display","flex");
            for (var i=0; i<750; i+=2) {
            x=i/10;
            i=i-1;

             $("#bar").css("width",x+"%");
}
           $("#calculaReporte").submit();
            //process form here.
        } else {
            swal( fail_log );
        }
}
        
     

    </script>
    @endsection

   