   @php
   $suma_ingreso_consultor=0;
   $suma_ingreso_family=0;
   $suma_costo_final=0;
   $suma_costo_inicial=0;
   $suma_becas=array();
   @endphp
   <table class="table" id="table2excel">
      <thead class="thead-dark">
       <tr>
        <th scope="col">Citas Pagadas</th>
        <th scope="col">Fecha cita</th>
        <th scope="col">Fecha Pago</th>
        <th scope="col">Consultor</th>
        <th scope="col">Paciente</th>
        <th scope="col">Colegio</th>
        <th scope="col">Observaciones</th>
        <th scope="col">Ingreso Consultor</th>
        <th scope="col">Ingreso Family</th>
        <th scope="col">Costo Inicial</th>
        <th scope="col">Costo Final</th>

        @foreach($becas as $beca)
        @php 

        $suma_becas[$beca->descripcion]=0;

        @endphp
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1">{{$beca->descripcion}}</th>
        <th scope="col" colspan="1">$</th>
        @else
        <th scope="col" colspan="2">{{$beca->descripcion}}</th>
        @endif
        @endforeach
    </tr>
</thead>
<tbody>
    @if($citas_pagadas->count()==0)
    <tr><td colspan="5"><center><h1>Hay datos</h1></center> </td></tr>
    @else
   @foreach($citas_pagadas as $cita)
   <tr>
    <th scope="row">{{$cita->id}}</th>
    @php

    $fecha=$cita->start;
    $fecha_actual = new DateTime($fecha);
    $cadena_fecha_actual = $fecha_actual->format("d/m/Y");
        $fecha_=$cita->fecha_pago;
    $fecha_actual_ = new DateTime($fecha_);
    $cadena_fecha_actual_ = $fecha_actual_->format("d/m/Y");
    @endphp
    <td>{{$cadena_fecha_actual}}</td>
    <td>{{$cadena_fecha_actual_}}</td>
    <td>{{$cita->consultor->nombrecompleto}}</td>
    <td>{{$cita->paciente->nombrecompleto}}</td>

    <td>{{($cita->paciente->colegio==0)?'':$cita->paciente->colegio_s->descripcion}}</td>
    <td>{{$cita->descripcion}}</td>
     <td>${{number_format($cita->ingreso_consultor,2)}}</td>
    <td>${{number_format($cita->ingreso_family,2)}}</td>
    <td>${{number_format($cita->monto_inicial,2)}}</td>
    <td>${{number_format($cita->monto_final,2)}}</td>
    @php
    $suma_ingreso_consultor=$suma_ingreso_consultor+$cita->ingreso_consultor;
    $suma_ingreso_family=$suma_ingreso_family+$cita->ingreso_family;
    $suma_costo_inicial=$suma_costo_inicial+$cita->monto_inicial;
    $suma_costo_final=$suma_costo_final+$cita->monto_final;
    @endphp
    @foreach($becas as $bec_aux)
    @php
    $x=0;
    @endphp
    @foreach($cita->becas as $beca_cita)
    @if($beca_cita->Becas_Agregadas->beca_id==$bec_aux->id)

    @if($beca_cita->monto_porcentaje!='monto')
    <td colspan="1" >{{$beca_cita->Becas_Agregadas->cantidad*100}}%</td>
    <td colspan="1">{{"$".number_format($beca_cita->Becas_Agregadas->equivalente,2)}}
        @php 
        $aux=$suma_becas[$bec_aux->descripcion];
        $suma_becas[$bec_aux->descripcion]=$aux+$beca_cita->Becas_Agregadas->equivalente;

        @endphp
    </td>
    @php $x=0; break; @endphp
    @else
    <td colspan="2">${{number_format($beca_cita->Becas_Agregadas->equivalente,2)}}</td>
    @php 
    $suma_becas[$bec_aux->descripcion]=$suma_becas[$bec_aux->descripcion]+$beca_cita->Becas_Agregadas->equivalente;

    $x=0;
break;
    @endphp
    @endif
    @else
    @php $x=1;  @endphp
    @endif
    
    @endforeach
    @if($x==1)
           @if($bec_aux->monto_porcentaje!='monto')
        <td colspan="1">%0</td>
        <td colspan="1">$0</td>
        @else
        <td colspan="2">$0</td>
        @endif
    @endif
    @endforeach
</tr>
@endforeach
<tr style="background: #2368b3">
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col">${{number_format($suma_ingreso_consultor,2)}}</th>
    <th scope="col">${{number_format($suma_ingreso_family,2)}}</th>
    <th scope="col">${{number_format($suma_costo_inicial,2)}}</th>
    <th scope="col">${{number_format($suma_costo_final,2)}}</th>

    @foreach($becas as $beca)
    @php 
    @endphp
    @if($beca->monto_porcentaje!='monto')
    <th scope="col" colspan="1"></th>
    <th scope="col" colspan="1">{{"$".number_format($suma_becas[$beca->descripcion],2)}}</th>
    @else
    <th scope="col" colspan="2">{{"$".number_format($suma_becas[$beca->descripcion],2)}}</th>
    @endif
    @endforeach
</tr>
@endif
@php
$suma_ingreso_consultor=0;    
$suma_ingreso_family=0;    
$suma_costo_inicial=0;    
$suma_costo_final=0;    
    
    
@endphp
<tr class="thead-dark">
        <th scope="col">Citas Sin pagar</th>
        <th scope="col">Fecha cita</th>
        <th scope="col">Consultor</th>
        <th scope="col">Paciente</th>
    <th scope="col">Colegio</th>
    <th scope="col">Observaciones</th>
        <th scope="col">Ingreso Consultor</th>
        <th scope="col">Ingreso Family</th>
        <th scope="col">Costo Inicial</th>
        <th scope="col">Costo Final</th>

        @foreach($becas as $beca)
        @php 

        $suma_becas[$beca->descripcion]=0;

        @endphp
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1">{{$beca->descripcion}}</th>
        <th scope="col" colspan="1">$</th>
        @else
        <th scope="col" colspan="2">{{$beca->descripcion}}</th>
        @endif
        @endforeach
    </tr>
    @if($citas_sin_paga->count()==0)
    <tr><td colspan="5"><center><h1>Hay datos</h1></center> </td></tr>
    @else
@foreach($citas_sin_paga as $cita)
 <tr>
    <th scope="row">{{$cita->id}}</th>
    @php

    $fecha=$cita->start;
    $fecha_actual = new DateTime($fecha);
    $cadena_fecha_actual = $fecha_actual->format("d/m/Y");
    @endphp
    <td>{{$cadena_fecha_actual}}</td>
    <td>{{$cita->consultor->nombrecompleto}}</td>
    <td>{{$cita->paciente->nombrecompleto}}</td>
    <td>{{($cita->paciente->colegio==0)?'':$cita->paciente->colegio_s->descripcion}}</td>
    <td>{{$cita->descripcion}}</td>
     <td>${{number_format($cita->ingreso_consultor,2)}}</td>
    <td>${{number_format($cita->ingreso_family,2)}}</td>
    <td>${{number_format($cita->monto_inicial,2)}}</td>
    <td>${{number_format($cita->monto_final,2)}}</td>
    @php
    $suma_ingreso_consultor=$suma_ingreso_consultor+$cita->ingreso_consultor;
    $suma_ingreso_family=$suma_ingreso_family+$cita->ingreso_family;
    $suma_costo_inicial=$suma_costo_inicial+$cita->monto_inicial;
    $suma_costo_final=$suma_costo_final+$cita->monto_final;
    @endphp
    @foreach($becas as $bec_aux)
    @php
    $x=0;
    @endphp
    @foreach($cita->becas as $beca_cita)
    @if($beca_cita->Becas_Agregadas->beca_id==$bec_aux->id)

    @if($beca_cita->monto_porcentaje!='monto')
    <td colspan="1" >{{$beca_cita->Becas_Agregadas->cantidad*100}}%</td>
    <td colspan="1">{{"$".number_format($beca_cita->Becas_Agregadas->equivalente,2)}}
        @php 
        $aux=$suma_becas[$bec_aux->descripcion];
        $suma_becas[$bec_aux->descripcion]=$aux+$beca_cita->Becas_Agregadas->equivalente;

        @endphp
    </td>
    @php $x=0; break; @endphp
    @else
    <td colspan="2">${{number_format($beca_cita->Becas_Agregadas->equivalente,2)}}</td>
    @php 
    $suma_becas[$bec_aux->descripcion]=$suma_becas[$bec_aux->descripcion]+$beca_cita->Becas_Agregadas->equivalente;

    $x=0; break;

    @endphp
    @endif
    @else

    @php $x=1; @endphp
    @endif
    @endforeach
    @if($x==1)
           @if($beca_cita->monto_porcentaje!='monto')
        <td colspan="1"></td>
        <td colspan="1"></td>
        @else
        <td colspan="2"></td>
        @endif
    @endif
    @endforeach

</tr>
@endforeach
 <tr style="background: #2368b3">
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>   
    <th scope="col"></th>
    <th scope="col">${{number_format($suma_ingreso_consultor,2)}}</th>
    <th scope="col">${{number_format($suma_ingreso_family,2)}}</th>
    <th scope="col">${{number_format($suma_costo_inicial,2)}}</th>
    <th scope="col">${{number_format($suma_costo_final,2)}}</th>

    @foreach($becas as $beca)
    @php 
    @endphp
    @if($beca->monto_porcentaje!='monto')
    <th scope="col" colspan="1"></th>
    <th scope="col" colspan="1">{{"$".number_format($suma_becas[$beca->descripcion],2)}}</th>
    @else
    <th scope="col" colspan="2">{{"$".number_format($suma_becas[$beca->descripcion],2)}}</th>
    @endif
    @endforeach
</tr>
@endif
</tbody>
</table>
<button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
</div>
<script type="text/javascript">

    $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

    name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

});

    });
</script>
