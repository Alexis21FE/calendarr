    @extends('layouts.app')
    @section('content')
    @php
    $x=0;
    if(isset($isResponse)){
        $fecha_ini=$inicio;
        $apoyo_id=$apoyo_id;
    }else{
           $fecha_ini=null;
        $apoyo_id=null;
    }
    @endphp
    <center>

        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Estado de cuenta</p>
    </center>
    <div class="section">
        {{Form::open(['route'=>'reporte.consultaApoyos','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
        <div class="row" style="margin:auto;">

            <div class="col">
                {{Form::label('fecha_ini','Fecha Inicial')}}
                {{Form::date('fecha_ini',$fecha_ini,['class'=>'form-control','type'=>'date','required'=>'required','id'=>'fecha_ini','onChange'=>'calculaReporte()'])}}
            </div>
            <div class="col">
                {{Form::label('fecha_fin','Fecha final')}}
                {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required'])}}

            </div>
            <div class="col">
                {{Form::label('apoyo','Selecciona Apoyo')}}
                {{Form::select('apoyo_id',$apoyos->pluck('descripcion','id'),$apoyo_id,['id'=>'select_apoyo','class'=>'form-control','placeholder'=>'Selecciona un apoyo','onChange'=>'calculaReporte()'])}}
            </div>
        </div>
        {{Form::close()}}
        <hr>
         <div id="response">
        @if(isset($isResponse))
       
        <table class="table" id="table2excel">
          <thead class="thead-dark">
             <tr>
                <th scope="col">Apoyo</th>
                <th scope="col">Paciente</th>
                <th scope="col">Consultor</th>
                <th scope="col">Fecha</th>
                <th scope="col">Saldo Inicial</th>
                <th scope="col">Beneficio</th>
                <th scope="col">Saldo final</th>
                <th scope="col">Colegio</th>
            </tr>
        </thead>
        <tbody>
         @foreach($registro_apoyos as $registro_apoyo)
         @php
         $fecha=$registro_apoyo->citas_re->fecha_cita;
         $fecha_actual = new DateTime($fecha);
         $cadena_fecha_actual = $fecha_actual->format("d/m/Y");
         @endphp
         <tr>
            <td>{{$registro_apoyo->apoyo_re->descripcion}}</td>
            <!--td>/*empty($registro_apoyo->citas_re->paciente->alias)?$registro_apoyo->citas_re->paciente->alias:$registro_apoyo->citas_re->paciente->nombre*/}}</td-->
            <td>{{$registro_apoyo->citas_re->paciente->nombrecompleto}}</td>
            <td>{{$registro_apoyo->citas_re->consultor->nombre}}</td>
            <td>{{$cadena_fecha_actual}}</td>
            <th>${{$registro_apoyo->saldo_inicial}}</th>
            <th>${{$registro_apoyo->cargo}}</th>
            <th>${{$registro_apoyo->saldo_final}}</th>
            <th>{{empty($registro_apoyo->citas_re->paciente->colegio)?'':$registro_apoyo->citas_re->paciente->colegio_s->descripcion}}</th>
        </tr>
        @endforeach
    </tbody>
</table>
<button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
</div>
@endif
</div>
<script type="text/javascript">

    $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

    name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

});

    });

        function calculaReporte(){
            if($("#fecha_ini").val()!=''){
                if($("#select_apoyo").val()!=''){
                     $("#response").empty();
                 $('<div class="progress" id="gif" style="width:30%;display: none;">'+
  '<div id="bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 40%">Espera un momento...</div>').insertBefore("#response");
            $("#gif").css("display","flex");
            for (var i=0; i<750; i+=2) {
            x=i/10;
            i=i-1;

             $("#bar").css("width",x+"%");
}
                    $("#calculaReporte").submit();

                }else{
                swal("Selecciona un Apoyo por favor");
              }   

          }else{
            swal("Selecciona una Fecha por favor");
          }
      }


</script>
@endsection