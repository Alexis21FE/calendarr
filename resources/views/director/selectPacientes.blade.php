	@extends('../layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			{{Form::label('consultor','Consultor')}}
                {{Form::select('consultor',$consultores->pluck('nombrecompleto','id'),null,['class'=>'form-control','id'=>'consultor_select','placeholder'=>'Selecciona un consultor'])}}</div>
		
		
		<div class="col">
			{{Form::label('pacientes','Pacientes')}}
			<select class="form-control "  id="select_Paciente">

		  <option value="" disabled selected hidden>Elija un paciente</option>
		@if(!empty($pacientes))
		@foreach($pacientes as $paciente)
		<option value="{{$paciente->id}}" style="background-color: {{$paciente->color}};color:black;">
			{{!empty($paciente->alias)?$paciente->alias." - Pac ".$paciente->nombreconsultor :$paciente->nombre." ".$paciente->apellido_pa." ".$paciente->apellido_ma." - Pac ".$paciente->nombreconsultor}}
		</option>
		@endforeach
		@endif
	</select>
		</div>
		<div class="col"></div>
		<div class="col"></div>
		</div>
	
	
	<hr>
	
<div id="response">
</div>
</div>
<script type="text/javascript">

		$("#select_Paciente").change(function(){
			var paciente_id=$("#select_Paciente").val();
			$.ajax({
				url:"{{route('paciente.Showid')}}",
				data:{
					paciente_id:paciente_id
				},
				datatype:'json',
				type:'post',
				success:function(response){
					$("#response").empty();
					$("#gif").remove();
					$("#response").html(response);
				},
				error:function(){},

			});
		});

		              $("#consultor_select").change(function(){
                   $("#response").empty();
                $.ajax({
                  url:"{{route('dame.pacientes')}}",
                  datatype:"json",
                  type:"post",
                  data:{consultor:$(this).val()},
                  success:function(response){

                    $("#gif").remove();
                          $("#select_Paciente").empty();
                           $("#select_Paciente").append('<option>Selecciona un paciente</option>');
       
            $(response.pacientes).each(function(index,pac){

              if(pac.apellido_pa!=''){
                var ap_pa=pac.apellido_pa;
              }else
              {
                 ap_pa='';
              }if(pac.apellido_ma!=''){
                var ap_ma=pac.apellido_ma;
              }else
              {
                 ap_ma='';
              }

          $("#select_Paciente").append('<option style="background:'+pac.color+' " value="'+ pac.id +'">'+ pac.nombrecompleto+"- Pac "+pac.nombreconsultor+'</option>');
        });
                  },
                  error:function(){}
                });
              });

</script>
@endsection