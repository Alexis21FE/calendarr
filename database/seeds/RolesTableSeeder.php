<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\sedes;
use Carbon\Carbon;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now=Carbon::now(); 
        $Role =new Role();
         $Role->name='admin';
         $Role->display_name='Administradora';
         $Role->save();
         $Role =new Role();
         $Role->name='direc';
         $Role->display_name='Directora';
         $Role->save();
         $Role =new Role();
         $Role->name='consul';
         $Role->display_name='Consultor';
         $Role->save();    
         $Role =new Role();
         $Role->name='recep';
         $Role->display_name='Recepcionista';
         $Role->save();
       
       $sede= new sedes();
        $sede->descripcion='Liceo del valle';
         $sede->fecha_modificacion=$now;
        $sede->save();
       $sede= new sedes();
        $sede->descripcion='Torreblanca';
        $sede->fecha_modificacion=$now;
        $sede->save();
       $sede= new sedes();
        $sede->descripcion='Tzitlacalli Modena';
        $sede->fecha_modificacion=$now;
        $sede->save();
       $sede= new sedes();
        $sede->descripcion='Altamira Moctezuma';
        $sede->fecha_modificacion=$now;
        $sede->save();
       $sede= new sedes();
        $sede->descripcion='Altamira La Cima';
        $sede->fecha_modificacion=$now;
        $sede->save();
        
        
        

    }
}

