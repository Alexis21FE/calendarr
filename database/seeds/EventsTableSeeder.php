<?php

use Illuminate\Database\Seeder;
use App\event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Citas::Class,2)->create();
        factory(App\User::class,5)->create();
        factory(App\Pacientes::class,2)->create();
        factory(App\Consultorio::Class,10)->create();
    }
}
