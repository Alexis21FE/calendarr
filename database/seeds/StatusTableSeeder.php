<?php

use Illuminate\Database\Seeder;
use App\Status;
use App\Especialidades;
use App\Tipo_citas;
use App\Beca_descuento;
use Carbon\Carbon;
use App\status_cita;
use App\apoyo;
class StatusTableSeeder extends Seeder
{
    
    public function run()
    {
        $now=Carbon::now();
         $status =new Status();
         $status->descripcion='Activo';
         $status->save();
         $status =new Status();
         $status->descripcion='Diagnostico';
         $status->save();
         $status =new Status();
         $status->descripcion='Alta';
         $status->save();
         $tipo_cita=new Tipo_citas;
         $tipo_cita->descripcion='seguimiento';
         $tipo_cita->fecha_modificacion=$now;
         $tipo_cita->save();         
         $tipo_cita=new Tipo_citas;
         $tipo_cita->descripcion='diagnostico ';
         $tipo_cita->fecha_modificacion=$now;
         $tipo_cita->save();         
         $tipo_cita=new Tipo_citas;
         $tipo_cita->descripcion='voluntariado';
         $tipo_cita->fecha_modificacion=$now;
         $tipo_cita->save();
         $beca_descuento=new Beca_descuento;
         $beca_descuento->descripcion='Descuentos';
         $beca_descuento->afecta='ambos';
         $beca_descuento->tiene_saldo='N';
         $beca_descuento->orden=1;
         $beca_descuento->monto_porcentaje='monto';
         $beca_descuento->save();
                  $beca_descuento=new Beca_descuento;
         $beca_descuento->descripcion='Descuento consultor';
         $beca_descuento->afecta='consultor';
         $beca_descuento->tiene_saldo='N';
         $beca_descuento->orden=4;
         $beca_descuento->monto_porcentaje='monto';
         $beca_descuento->save();
         
         $beca_descuento=new Beca_descuento;
         $beca_descuento->descripcion='Apoyo colmenares';
         $beca_descuento->afecta='institucion';
         $beca_descuento->tiene_saldo='S';
         $beca_descuento->orden=3;
         $beca_descuento->monto_porcentaje='porcentaje';
         $beca_descuento->save();
         
         $beca_descuento=new Beca_descuento;
         $beca_descuento->descripcion='Beca Family';
         $beca_descuento->afecta='ambos';
         $beca_descuento->tiene_saldo='N';
         $beca_descuento->orden=2;
         $beca_descuento->monto_porcentaje='porcentaje';
         $beca_descuento->save();

         $status_cita=new status_cita;
         $status_cita->descripcion='programada';
         $status_cita->fecha_modificacion=$now;
         $status_cita->save();         
         $status_cita=new status_cita;
         $status_cita->descripcion='cancelada';
         $status_cita->fecha_modificacion=$now;
         $status_cita->save();         
         $status_cita=new status_cita;
         $status_cita->descripcion='pagada';
         $status_cita->fecha_modificacion=$now;
         $status_cita->save();         
         $status_cita=new status_cita;
         $status_cita->descripcion='reasignacion';
         $status_cita->fecha_modificacion=$now;
         $status_cita->save();         
         $status_cita=new status_cita;
         $status_cita->descripcion='confirmada';
         $status_cita->fecha_modificacion=$now;
         $status_cita->save();         
         $status_cita=new status_cita;
         $status_cita->descripcion='sin pagar';
         $status_cita->fecha_modificacion=$now;
         $status_cita->save();
         
   

         

	}
}
