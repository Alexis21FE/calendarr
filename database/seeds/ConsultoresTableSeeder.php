<?php

use Illuminate\Database\Seeder;
use App\consultor_categoria;
use App\Especialidades;
use App\Consultores;
use Carbon\Carbon;
class ConsultoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now=Carbon::now();
    	$categoria= new consultor_categoria;
    	$categoria->descripcion='A1';
    	$categoria->fecha_modificacion=$now;
        $categoria->porcentaje_consultor=70;
        $categoria->porcentaje_family=30;
        $categoria->save();
        $categoria= new consultor_categoria;
        $categoria->descripcion='B1';
        $categoria->fecha_modificacion=$now;
        $categoria->porcentaje_consultor=70;
        $categoria->porcentaje_family=30;
        $categoria->save();
        $categoria= new consultor_categoria;
        $categoria->descripcion='B2';
        $categoria->fecha_modificacion=$now;
        $categoria->porcentaje_consultor=56;
        $categoria->porcentaje_family=44;
        $categoria->save(); 
         $categoria= new consultor_categoria;
        $categoria->descripcion='B3';
        $categoria->fecha_modificacion=$now;
        $categoria->porcentaje_consultor=50;
        $categoria->porcentaje_family=50;
        $categoria->save();  
        $categoria= new consultor_categoria;
        $categoria->descripcion='B4';
        $categoria->fecha_modificacion=$now;
        $categoria->porcentaje_consultor=0;
        $categoria->porcentaje_family=100;
        $categoria->save();
        $categoria= new consultor_categoria;
        $categoria->descripcion='C1';
        $categoria->fecha_modificacion=$now;
        $categoria->porcentaje_consultor=50;
        $categoria->porcentaje_family=50;
        $categoria->save();
        $categoria= new consultor_categoria;
        $categoria->descripcion='C2';
        $categoria->fecha_modificacion=$now;
        $categoria->porcentaje_consultor=0;
        $categoria->porcentaje_family=100;
        $categoria->save();

        $especialidad= new Especialidades;
        $especialidad->descripcion='Asesoria Personal (Voluntariado)';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Asesoria Familiar (voluntariado)';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Coaching vocacional';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Pedagogia (dinamicas familiares)';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Tanatologia (duelos y pérdidas)';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Psicoterapia Personal';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Psicoterapia Familiar';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Psicoterapia Pareja';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Psicoterapia  infantil';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Consultoria Personal';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Consultoria Familiar';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();
        $especialidad= new Especialidades;
        $especialidad->descripcion='Consultoria Pareja';
        $especialidad->fecha_modificacion=$now;
        $especialidad->save();   

       /* $usuario = User::create([
            'name'=>'Alejandra' , 
            'email'=>'ampudiacar@gmail.com',
            'password'=> bcrypt('123456'),
            'role_id'=>3
        ]);
        $consultor=new Consultores;
        $consultor->user_id=$usuario->id;
        $consultor->color='#9EA220';
        $consultor->nombre= 'Alejandra';
        $consultor->apellido_pa= 'Ampudia';
        $consultor->apellido_ma= 'Cardenas'; 
        $consultor->celular_paciente='3335784548';
        $consultor->telefono_paciente='33112255';
        $consultor->direccion_paciente='none';
        $consultor->numero_ext=0;
        $consultor->numero_int=0;
        $consultor->colonia='null';
        $consultor->carrera='Maestra Montesori';
        $consultor->categoria_id=7;
 $consultor->diagnostico='N';
 $consultor->diagnostico_costo=0;
 $consultor->voluntariado='S';
 $consultor->voluntariado_costo=0;
 $consultor->costo_individual=0;
 $consultor->costo_familiar=0;
 $consultor->fecha_registro_consultor=$now;
 $consultor->fecha_modificacion=$now;
 $consultor->save();
       //Inserta en la Tabla horarios
 $horario_consultor=new horario_consultor;
 for ($i=1; $i <=5 ; $i++) { 
    $diagnostico='dia'.''.$i;
    $hora_entrada='hora_entrada'.''.$i;
    $hora_salida ='hora_salida'.''.$i;
    $day=$request->$dia;
    if(!isset($day)){
    }else{
        $dia_entrada=$day.'_entrada';
        $dia_salida=$day.'_salida';
        $horario_consultor->$dia_entrada=$hora_entrada;
        $horario_consultor-> $dia_salida=$hora_salida;
    }
}
$horario_consultor->consultor_id=$consultor->id;
$horario_consultor->save();

//inserta en la tabla consultor_especialidads

foreach ($request->especialidades as $key => $value) {
    $consultor_especialidad= new consultores_especialidad;
    $consultor_especialidad->especialidades_id=$value;
    $consultor_especialidad->consultores_id=$consultor->id;
    $consultor_especialidad->save();
}
    }*/
} }       

