<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBecaDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beca_descuentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->enum('afecta',['institucion','consultor','ambos']);
            $table->integer('orden');
            $table->enum('tiene_saldo',['S','N']);
            $table->enum('monto_porcentaje',['monto','porcentaje']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beca_descuentos');
    }
}
