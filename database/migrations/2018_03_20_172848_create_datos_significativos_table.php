<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosSignificativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_significativos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_paciente');
            $table->integer('id_caso');
            $table->integer('Cambio de residencia');
            $table->integer('Accidentes');
            $table->integer('Muertes');
            $table->integer('Separacion');
            $table->integer('Divorcio');
            $table->integer('Escolar');
            $table->integer('Perdida_empleo');
            $table->integer('Enfermedades');
            $table->integer('Economico');
            $table->integer('Asalto');
            $table->integer('Carcel');
            $table->integer('Embarazo');
            $table->integer('Genetico');
            $table->integer('Otros');
            $table->date('fecha_modificacion');
            $table->integer('id_usuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_significativos');
    }
}
