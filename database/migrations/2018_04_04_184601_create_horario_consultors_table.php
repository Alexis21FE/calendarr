<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorarioConsultorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario_consultors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consultor_id');
            $table->time('lunes_entrada')->default('00:00:00')->nullable();
            $table->time('lunes_salida')->default('00:00:00')->nullable();
            $table->time('martes_entrada')->default('00:00:00')->nullable();
            $table->time('martes_salida')->default('00:00:00')->nullable();
            $table->time('miercoles_entrada')->default('00:00:00')->nullable();
            $table->time('miercoles_salida')->default('00:00:00')->nullable();
            $table->time('jueves_entrada')->default('00:00:00')->nullable();
            $table->time('jueves_salida')->default('00:00:00')->nullable();
            $table->time('viernes_entrada')->default('00:00:00')->nullable();
            $table->time('viernes_salida')->default('00:00:00')->nullable();
            $table->time('sabado_entrada')->default('00:00:00')->nullable();
            $table->time('sabado_salida')->default('00:00:00')->nullable();
            $table->integer('horas_disponible');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario_consultors');
    }
}
