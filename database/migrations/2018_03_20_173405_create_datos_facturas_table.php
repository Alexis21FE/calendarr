<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paciente_id')->nullable();
            $table->string('RFC')->nullable();
            $table->string('facturar_a')->nullable();
            $table->string('domicilio_factura')->nullable();
            $table->integer('cp_factura')->nullable();
            $table->string('ciudad_factura')->nullable();
            $table->string('colonia_factura')->nullable();
            $table->integer('uso_cfdi')->nullable();
            $table->datetime('fecha_modificacion')->nullable();
            $table->integer('id_usuario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_facturas');
    }
}

