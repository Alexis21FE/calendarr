<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paciente_id')->nullable();
            $table->integer('caso_id')->nullable();
            $table->integer('consultor_id')->nullable();
            $table->integer('consultorio_id')->nullable();
            $table->integer('tipo_cita_id')->nullable();
            $table->integer('status_cita_id')->nullable();
            $table->integer('sesion')->nullable();
            $table->string('cita_id_google')->nullable();
            $table->datetime('fecha_modificacion')->nullable();
            $table->integer('diagnostico_costo')->nullable();
            $table->integer('voluntariado_costo')->nullable();
            $table->enum('individual_pareja',['Individual','Pareja'])->nullable();
            $table->integer('id_usuario')->nullable();
            $table->float('monto_inicial')->nullable();
            $table->float('monto_final')->nullable();
            $table->float('ingreso_consultor')->nullable();
            $table->float('ingreso_family')->nullable();
            $table->date('fecha_cita')->nullable();
            $table->time('hora')->nullable();
            $table->string('title')->nullable();
            $table->string('descripcion')->nullable();
            $table->datetime('fecha_pago')->nullable();
            $table->datetime('start')->nullable();
            $table->datetime('end')->nullable();
            $table->string('color',7)->nullable();

            $table->timestamps();
        });
    }
            



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
