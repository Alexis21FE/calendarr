<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultorCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultor_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->float('porcentaje_consultor');
            $table->float('porcentaje_family');
            $table->datetime('fecha_modificacion');
            $table->integer('id_usuario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultor_categorias');
    }
}
