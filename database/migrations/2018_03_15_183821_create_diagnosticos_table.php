<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosticos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consultor_id');
            $table->integer('paciente_id');
            $table->integer('clasificacion_id');
            $table->date('fecha_diagnostico');
            $table->enum('medicamento',['S','N']);
            $table->string('causa_medicamento')->nullable();
            $table->string('razon_ingreso');
            $table->integer('familia_id')->nullable();
            $table->integer('datos_significantivos_id')->nullable();
            $table->integer('num_sesiones');
            $table->string('hipotesis');
            $table->integer('caso_id')->nullable();
            $table->integer('consultor_asig_id');
            $table->date('fecha_modificacion')->nullable();
            $table->integer('id_usuario')->nullable();
            $table->integer('cita_id')->nullable();
            $table->timestamps();
        });
    }

    /* Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosticos');
    }
}

